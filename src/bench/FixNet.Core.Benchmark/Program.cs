﻿namespace FixNet.Core.Benchmark
{
    using BenchmarkDotNet.Running;

    public static class Program
    {
        static void Main(string[] args)
        {
            BenchmarkRunner.Run<MessageCopying>();
            //BenchmarkRunner.Run<MessageBufferTiming>();
        }
    }
}
