﻿namespace FixNet.Core.Benchmark
{
    using BenchmarkDotNet.Configs;
    using BenchmarkDotNet.Diagnostics.Windows;
    using BenchmarkDotNet.Jobs;

    public class MemoryDiagnosingConfig : ManualConfig
    {
        public MemoryDiagnosingConfig()
        {
            Add(new MemoryDiagnoser());
            Add(Job.RyuJitX64, Job.LegacyJitX86);
        }
    }
}