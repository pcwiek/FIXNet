﻿namespace FixNet.Core.Benchmark
{
    using System;
    using BenchmarkDotNet.Attributes;
    using Fields;
    using Messages;

    [Config(typeof(MemoryDiagnosingConfig))]
    public class MessageCopying
    {
        private readonly Message message;
        public MessageCopying()
        {
            message = new Message();
            message.Set(new Field.Int(111, 123));
            message.Set(new Field.Int(112, 1));
            message.Set(new Field.Int(113, 13));
            message.Set(new Field.Decimal(32, 11m));
            message.Set(new Field.Decimal(33, 882.25m));
            message.Set(new Field.String(82, "AAAAAAAAAAAAAAAAA"));
            message.Set(new Field.String(84, "BBBB-CCCCCCCCCCC-12311-AA"));
            message.Set(new Field.String(221, "Test-1231-Test"));
            var group = message.GetGroup(666);
            for (var i = 0; i < 5; i++)
            {
                var item = new GroupItem();
                item.Set(new Field.String(776, "BBBB"+i));
                item.Set(new Field.String(882, "AABWBDBA2@@@3312"+i));
                item.Set(new Field.DateTime(9918, DateTime.UtcNow, true));
                group.Add(item);
            }
        }
             

        [Benchmark]
        public Message DeepCopy()
        {
            return new Message(message);
        }

        [Benchmark(Baseline = true)]
        public Message ShallowCopy()
        {
            return new Message(message, CopyType.Shallow);
        }
    }
}
