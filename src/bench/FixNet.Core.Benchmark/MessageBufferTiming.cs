﻿namespace FixNet.Core.Benchmark
{
    using System.Text;

    using BenchmarkDotNet.Attributes;

    [Config(typeof(MemoryDiagnosingConfig))]
    public class MessageBufferTiming
    {
        private const string Msg =
            "8=FIX.4.49=15535=D34=21549=CLIENT4452=20060425-19:42:55.71856=TW441=juinternal11=1234621=138=140=244=554=155=EAS59=060=20060425-14:42:55207=ZZ461=ESXXXX10=013";

        private readonly byte[] messageBytes = Encoding.UTF8.GetBytes(Msg);
        private readonly MessageBuffer buffer = new MessageBuffer();

        [Benchmark]
        public string ExtractingMessages()
        {
            buffer.AddChunk(messageBytes);
            return buffer.ExtractMessage();
        }
    }
}