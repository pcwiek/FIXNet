﻿namespace FixNet.Core.Benchmark
{
    using System.Collections.Generic;
    using BenchmarkDotNet.Attributes;
    using Fields;
    using Messages;
    using Parsing;
    using Specification;

    [Config(typeof(MemoryDiagnosingConfig))]
    public class MessageBuildingTiming
    {
        private const string Msg =
            "8=FIX.4.49=15535=D34=21549=CLIENT4452=20060425-19:42:55.71856=TW441=juinternal11=1234621=138=140=244=554=155=EAS59=060=20060425-14:42:55207=ZZ10=013";

        private readonly SpecificationData specData = new SpecificationData(
            new Dictionary<int, SpecField>
            {
                [1] = new SpecField(1, "Account", FieldType.String),
                [11] = new SpecField(11, "ClOrdId", FieldType.String),
                [21] = new SpecField(21, "HandlInst", FieldType.String),
                [38] = new SpecField(38, "OrderQty", FieldType.Qty),
                [40] = new SpecField(40, "OrdType", FieldType.Int),
                [44] = new SpecField(44, "Price", FieldType.Price),
                [54] = new SpecField(54, "Side", FieldType.Int),
                [55] = new SpecField(55, "Symbol", FieldType.String),
                [59] = new SpecField(59, "TimeInForce", FieldType.Int),
                [60] = new SpecField(60, "TransactTime", FieldType.UtcTimestamp),
                [207] = new SpecField(207, "SecurityExchange", FieldType.Exchange),
            },
            new Dictionary<MsgType, SpecElement>
            {
                [new MsgType("D")] = new SpecElement(new [] {1,11,21,38,40,44,54,55,59,60,207}, new [] {1,11,38,40,44,54,55})
            },
            new SpecElement(new[] {8, 9, 35, 34, 49, 52, 56}, new[] {8, 9, 35, 34}),
            new SpecElement(new[] {10}, new[] {10}));

        private readonly MessageBuilder builder = new MessageBuilder();

        private readonly MessageBuilder fullyVerifyingBuilder;
        private readonly MessageBuilder onlyHeaderFieldOrderBuilder;
        private readonly MessageBuilder onlyRequiredFieldsBuilder;
        private readonly MessageBuilder specDataWithoutVerificationBuilder;

        public MessageBuildingTiming()
        {
            fullyVerifyingBuilder = new MessageBuilder(specData, MessageBuilder.VerificationOptions.Full, new Dictionary<FieldType, CustomFieldParser>());
            onlyHeaderFieldOrderBuilder = new MessageBuilder(specData, MessageBuilder.VerificationOptions.CheckHeaderFieldOrder, new Dictionary<FieldType, CustomFieldParser>());
            onlyRequiredFieldsBuilder = new MessageBuilder(specData, MessageBuilder.VerificationOptions.CheckRequiredFields, new Dictionary<FieldType, CustomFieldParser>());
            specDataWithoutVerificationBuilder = new MessageBuilder(specData, MessageBuilder.VerificationOptions.None, new Dictionary<FieldType, CustomFieldParser>());
        }

        [Benchmark]
        public Message NoVerification()
        {
            return builder.Build(Msg);
        }

        [Benchmark]
        public Message FullyVerifyMessage()
        {
            return fullyVerifyingBuilder.Build(Msg);
        }

        [Benchmark]
        public Message CheckHeaderFieldOrderOnly()
        {
            return onlyHeaderFieldOrderBuilder.Build(Msg);
        }

        [Benchmark]
        public Message CheckRequiredFieldsOnly()
        {
            return onlyRequiredFieldsBuilder.Build(Msg);
        }

        [Benchmark]
        public Message NoVerificationWithSpec()
        {
            return specDataWithoutVerificationBuilder.Build(Msg);
        }
    }
}