﻿namespace FixNet.Core
{
    using System;
    using System.Diagnostics;
    using System.Text;
    using System.Threading;
    using System.Threading.Tasks;

    using Messages;

    using Transport;

    
    public partial class Session : ISession
    {
        private readonly TraceSource log = new TraceSource("Session");

        private readonly State state;

        private readonly Settings settings;
        
        private readonly ITransport transport;

        private readonly CancellationTokenSource runtimeCancellation;

        private Task executionTask;

        public event EventHandler<Message> NewMessage;

        private readonly MessageBuilder messageBuilder;

        protected Session(State state, ITransport transport, Settings settings)
        {
            this.state = state;
            runtimeCancellation = new CancellationTokenSource();
            this.transport = transport;
            this.settings = settings;
            messageBuilder = new MessageBuilder(
                settings.SpecificationData,
                settings.MessageVerificationOptions,
                settings.CustomFieldMapping);
        }

        private void Start()
        {
            if (runtimeCancellation.IsCancellationRequested)
            {
                return;
            }

            log.TraceInformation("Starting session");
            executionTask = ExecuteAsync();
        }

        private async Task ExecuteAsync()
        {
            var buffer = new MessageBuffer();
            var token = runtimeCancellation.Token;
            while (!runtimeCancellation.IsCancellationRequested)
            {
                var result = await transport.ReadAsync(token).ConfigureAwait(false);
                if (result.BytesRead > 0)
                {
                    buffer.AddChunk(result.Buffer);
                }

                var rawMessage = buffer.ExtractMessage();
                if (!string.IsNullOrEmpty(rawMessage))
                {
                    log.TraceEvent(TraceEventType.Verbose, 410, "Incoming message: {0}", rawMessage);
                    var message = messageBuilder.Build(rawMessage);
                    NewMessage?.Invoke(this, message);
                }
                await CheckStateAsync().ConfigureAwait(false);
            }
        }

        private async Task CheckStateAsync()
        {
            if (!state.HasFlag(State.SessionState.ReceivedLogon))
            {
                if (state.ShouldLogIn)
                {
                }
                else if (state.HasFlag(State.SessionState.SentLogon) && state.HasTimedOut(state.LogonTimeout))
                {
                    log.TraceEvent(TraceEventType.Warning, 420, "Timeout: no login response.");
                    await StopAsync().ConfigureAwait(false);
                }
            }

            if (state.HasTimedOut(state.Heartbeat))
            {
                log.TraceEvent(TraceEventType.Warning, 420, "Timeout: no data received within heartbeat period.");
                await StopAsync().ConfigureAwait(false);
            }
        }

        public Task SendAsync(Message message, CancellationToken cancellationToken)
        {
            var stringified = message.ToString();
            return transport.WriteAsync(Encoding.UTF8.GetBytes(stringified), cancellationToken);
        }

        public Task SendAsync(Message message)
        {
            return SendAsync(message, CancellationToken.None);
        }

        public async Task StopAsync()
        {
            log.TraceInformation("Stopping session");
            runtimeCancellation.Cancel();
            await transport.StopAsync().ConfigureAwait(false);
            await executionTask.ConfigureAwait(false);
        }
    }
}
