﻿namespace FixNet.Core
{
    using System;

    public abstract partial class Session
    {
        protected internal class State
        {
            private readonly Func<DateTime> currentUtcDateTime;

            public State(bool isInitiator, Func<DateTime> currentUtcDateTime)
            {
                this.currentUtcDateTime = currentUtcDateTime;
                IsInitiator = isInitiator;
            }

            [Flags]
            public enum SessionState
            {
                IsEnabled = 1 << 0,
                ReceivedLogon = 1 << 1,
                ReceivedReset = 1 << 2,
                SentLogon = 1 << 3,
                SentLogout = 1 << 4,
                SentReset = 1 << 5
            }

            public SessionState CurrentState { get; private set; }

            private bool IsInitiator { get; }

            public DateTime LastReceived { get; private set; }

            public TimeSpan? LogonTimeout { get; private set; }

            public TimeSpan Heartbeat { get; private set; }

            public bool ShouldLogIn => IsInitiator && !HasFlag(SessionState.SentLogon);

            public bool HasFlag(SessionState state)
            {
                return (CurrentState & state) != 0;
            }

            public void AddFlag(SessionState state)
            {
                CurrentState |= state;
            }

            public void RefreshLastReceived()
            {
                LastReceived = currentUtcDateTime();
            }

            public void RemoveFlag(SessionState state)
            {
                CurrentState &= ~state;
            }

            public bool HasTimedOut(TimeSpan? timeout)
            {
                if (timeout.HasValue)
                {
                    return currentUtcDateTime() - LastReceived >= timeout.Value;
                }
                return false;
            }
        }
    }
}
