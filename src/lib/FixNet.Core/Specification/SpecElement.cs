﻿namespace FixNet.Core.Specification
{
    using System.Collections.Generic;

    public class SpecElement
    {
        private readonly HashSet<int> fieldTags;

        private readonly HashSet<int> requiredTags;

        public IEnumerable<int> RequiredTags { get; }

        public SpecElement(IEnumerable<int> fieldTags, IEnumerable<int> requiredTags)
        {
            this.fieldTags = new HashSet<int>(fieldTags);
            this.requiredTags = new HashSet<int>(requiredTags);
            RequiredTags = this.requiredTags;
        }

        public bool IsRequired(int tag) => requiredTags.Contains(tag);

        public bool ContainsField(int tag) => fieldTags.Contains(tag);
    }
}