﻿namespace FixNet.Core.Specification
{
    using System;

    /// <summary>
    /// Represents a FIX type.
    /// </summary>
    public struct FieldType : IEquatable<FieldType>
    {
        public bool Equals(FieldType other)
        {
            return string.Equals(Value, other.Value, StringComparison.OrdinalIgnoreCase);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is FieldType && Equals((FieldType)obj);
        }

        public override int GetHashCode()
        {
            return StringComparer.OrdinalIgnoreCase.GetHashCode(Value);
        }

        public static bool operator ==(FieldType left, FieldType right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(FieldType left, FieldType right)
        {
            return !left.Equals(right);
        }

        public string Value { get; }

        public FieldType(string value)
        {
            Value = value;
        }

        public override string ToString() => Value;

        public static readonly FieldType String = new FieldType("STRING");
        public static readonly FieldType MultipleValueString = new FieldType("MULTIPLEVALUESTRING");
        public static readonly FieldType MultipleStringValue = new FieldType("MULTIPLESTRINGVALUE");
        public static readonly FieldType MultipleCharValue = new FieldType("MULTIPLECHARVALUE");
        public static readonly FieldType Exchange = new FieldType("EXCHANGE");
        public static readonly FieldType LocalMkDate = new FieldType("LOCALMKDATE");
        public static readonly FieldType Data = new FieldType("DATA");
        public static readonly FieldType MonthYear = new FieldType("MONTHYEAR");
        public static readonly FieldType DayOfMonth = new FieldType("DAYOFMONTH");
        public static readonly FieldType Date = new FieldType("DATE");
        public static readonly FieldType Country = new FieldType("COUNTRY");
        public static readonly FieldType Boolean = new FieldType("BOOLEAN");
        public static readonly FieldType Char = new FieldType("CHAR");
        public static readonly FieldType Price = new FieldType("PRICE");
        public static readonly FieldType Amt = new FieldType("AMT");
        public static readonly FieldType Qty = new FieldType("QTY");
        public static readonly FieldType Float = new FieldType("FLOAT");
        public static readonly FieldType PriceOffset = new FieldType("PRICEOFFSET");
        public static readonly FieldType Percentage = new FieldType("PERCENTAGE");
        public static readonly FieldType Int = new FieldType("INT");
        public static readonly FieldType NumInGroup = new FieldType("NUMINGROUP");
        public static readonly FieldType SeqNum = new FieldType("SEQNUM");
        public static readonly FieldType Length = new FieldType("LENGTH");
        public static readonly FieldType UtcDate = new FieldType("UTCDATE");
        public static readonly FieldType UtcDateOnly = new FieldType("UTCDATEONLY");
        public static readonly FieldType UtcTimestamp = new FieldType("UTCTIMESTAMP");
        public static readonly FieldType TzTimestamp = new FieldType("TZTIMESTAMP");
        public static readonly FieldType Time = new FieldType("TIME");
        public static readonly FieldType TzTimeOnly = new FieldType("TZTIMEONLY");
        public static readonly FieldType UtcTimeOnly = new FieldType("UTCTIMEONLY");
    }
}
