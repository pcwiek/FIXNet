﻿namespace FixNet.Core.Specification
{
    /// <summary>
    /// Represents a field defined in the specification file.
    /// </summary>
    public class SpecField
    {
        public SpecField(int tag, string name, FieldType fieldType)
        {
            Tag = tag;
            Name = name;
            FieldType = fieldType;
        }

        /// <summary>
        /// Gets the field tag.
        /// </summary>
        public int Tag { get; }

        /// <summary>
        /// Gets the field name.
        /// </summary>
        public string Name { get; }

        /// <summary>
        /// Gets the field type.
        /// </summary>
        public FieldType FieldType { get; }
    }
}
