﻿namespace FixNet.Core.Specification
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml.Linq;

    using Fields;

    /// <summary>
    /// Represents the data in the spec file. Used for parsing, typing and validation.
    /// </summary>
    public class SpecificationData
    {
        /// <summary>
        /// Gets a mapping of a specification defined fields and their matching tags. 
        /// </summary>
        public IReadOnlyDictionary<int, SpecField> Fields { get; }

        /// <summary>
        /// Gets a mapping of specification defined messages and their message types.
        /// </summary>
        public IReadOnlyDictionary<MsgType, SpecElement> Messages { get; }

        /// <summary>
        /// Gets the specification defined header.
        /// </summary>
        public SpecElement Header { get; }

        /// <summary>
        /// Gets the specification defined trailer.
        /// </summary>
        public SpecElement Trailer { get; }

        public static SpecificationData Empty { get; } = new SpecificationData();

        private SpecificationData()
        {
            Fields = new Dictionary<int, SpecField>();
            Messages = new Dictionary<MsgType, SpecElement>();
        }

        internal SpecificationData(
            IReadOnlyDictionary<int, SpecField> fields,
            IReadOnlyDictionary<MsgType, SpecElement> messages,
            SpecElement header,
            SpecElement trailer)
        {
            if (fields == null)
            {
                throw new ArgumentNullException(nameof(fields));
            }

            if (messages == null)
            {
                throw new ArgumentNullException(nameof(messages));
            }

            Fields = fields;
            Messages = messages;
            Header = header;
            Trailer = trailer;
        }
        
        private class XmlSpecField
        {
            public XmlSpecField(SpecField field, bool isRequired)
            {
                Field = field;
                IsRequired = isRequired;
            }

            public SpecField Field { get; }
            public bool IsRequired { get; }
        }

        private static IEnumerable<XmlSpecField> ExtractFields(XContainer element, IReadOnlyDictionary<string, SpecField> fieldsByName)
        {
            return
                element.Elements("field")
                    .Select(
                        f =>
                        new XmlSpecField(fieldsByName[f.Attribute("name").Value], f.Attribute("required").Value == "Y"));
        }

        private static SpecElement ExtractSpecElement(XContainer element, IReadOnlyDictionary<string, SpecField> fieldsByName)
        {
            var fields = ExtractFields(element, fieldsByName).ToList();
            return new SpecElement(
                fields.Select(f => f.Field.Tag),
                fields.Where(f => f.IsRequired).Select(f => f.Field.Tag));
        }

        /// <summary>
        /// Creates a new <see cref="SpecificationData"/> using specified stream.
        /// </summary>
        /// <param name="stream">Stream to be read for XML data</param>
        /// <returns>An instance of <see cref="SpecificationData"/></returns>
        public static SpecificationData Create(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException(nameof(stream));
            }

            return Create(XElement.Load(stream));
        }

        public static SpecificationData Create(XElement xml)
        {
            var fields =
                xml.Element("fields")?
                    .Elements("field")
                    .Select(
                        f =>
                        new SpecField(
                            int.Parse(f.Attribute("number").Value),
                            f.Attribute("name").Value,
                            new FieldType(f.Attribute("type").Value)))
                    .ToDictionary(f => f.Tag) ?? new Dictionary<int, SpecField>();
            var fieldsByName = fields.Select(kvp => kvp.Value)
                .ToDictionary(kvp => kvp.Name, StringComparer.OrdinalIgnoreCase);

            var messages = xml.Element("messages")?.Elements("message").Select(
                m =>
                {
                    var specElement = ExtractSpecElement(m, fieldsByName);
                    var tpe = m.Attribute("msgtype").Value;
                    return new KeyValuePair<MsgType, SpecElement>(new MsgType(tpe), specElement);

                }).ToDictionary(kvp => kvp.Key, kvp => kvp.Value) ?? new Dictionary<MsgType, SpecElement>();
            var header = xml.Elements("header").Select(e => ExtractSpecElement(e, fieldsByName)).FirstOrDefault();
            var trailer = xml.Elements("trailer").Select(e => ExtractSpecElement(e, fieldsByName)).FirstOrDefault();

            return new SpecificationData(fields, messages, header, trailer);
        }
    }
}
