﻿namespace FixNet.Core.Parsing
{
    using System;

    /// <summary>
    /// Represents an untyped FIX field.
    /// </summary>
    internal struct UntypedField : IEquatable<UntypedField>
    {
        public bool Equals(UntypedField other)
        {
            return Tag == other.Tag && string.Equals(Value, other.Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is UntypedField && Equals((UntypedField)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (Tag * 397) ^ Value.GetHashCode();
            }
        }

        public static bool operator ==(UntypedField left, UntypedField right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(UntypedField left, UntypedField right)
        {
            return !left.Equals(right);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="UntypedField"/> class using specified tag and raw value.
        /// </summary>
        /// <param name="tag">Field's tag</param>
        /// <param name="value">Raw field's value</param>
        public UntypedField(int tag, string value)
        {
            Tag = tag;
            Value = value;
        }

        /// <summary>
        /// Gets the field's tag
        /// </summary>
        public int Tag { get; }

        /// <summary>
        /// Gets the raw field value
        /// </summary>
        public string Value { get; }
    }
}