﻿namespace FixNet.Core.Parsing
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;

    using Fields;

    using Specification;

    /// <summary>
    /// Allows to parse and assign types to fields from a raw FIX message.
    /// </summary>
    internal class Parser
    {
        private readonly TraceSource log = new TraceSource(nameof(Parser));

        /// <summary>
        /// Represents a field extraction result, which contains both the raw field and the next offset into the source string.
        /// </summary>
        private struct FieldExtractionResult : IEquatable<FieldExtractionResult>
        {
            public bool Equals(FieldExtractionResult other)
            {
                return RawField.Equals(other.RawField) && EndIndex == other.EndIndex;
            }

            public override bool Equals(object obj)
            {
                if (ReferenceEquals(null, obj)) return false;
                return obj is FieldExtractionResult && Equals((FieldExtractionResult)obj);
            }

            public override int GetHashCode()
            {
                unchecked
                {
                    return (RawField.GetHashCode() * 397) ^ EndIndex;
                }
            }

            public static bool operator ==(FieldExtractionResult left, FieldExtractionResult right)
            {
                return left.Equals(right);
            }

            public static bool operator !=(FieldExtractionResult left, FieldExtractionResult right)
            {
                return !left.Equals(right);
            }

            /// <summary>
            /// Initializes a new instance of the <see cref="FieldExtractionResult"/> struct using specified raw field and end index.
            /// </summary>
            /// <param name="rawField">Extracted field</param>
            /// <param name="endIndex">End index of the field in the source string</param>
            public FieldExtractionResult(UntypedField rawField, int endIndex)
            {
                RawField = rawField;
                EndIndex = endIndex;
            }

            /// <summary>
            /// Gets the extracted, untyped field
            /// </summary>
            public UntypedField RawField { get; }

            /// <summary>
            /// Gets the end index of the extracted field in the source string.
            /// </summary>
            public int EndIndex { get; }

            public static readonly FieldExtractionResult Unavailable = new FieldExtractionResult(new UntypedField(), -1);
        }

        private readonly SpecificationData specificationData;

        private static readonly HashSet<FieldType> StringFields = new HashSet<FieldType>
                                                                      {
                                                                          FieldType.String,
                                                                          FieldType.MultipleStringValue,
                                                                          FieldType.MultipleValueString,
                                                                          FieldType.Exchange,
                                                                          FieldType.MultipleCharValue,
                                                                          FieldType.LocalMkDate,
                                                                          FieldType.Data,
                                                                          FieldType.MonthYear,
                                                                          FieldType.DayOfMonth,
                                                                          FieldType.Date,
                                                                          FieldType.Country
                                                                      };

        private static readonly HashSet<FieldType> DecimalFields = new HashSet<FieldType>
                                                                       {
                                                                           FieldType.Price,
                                                                           FieldType.Amt,
                                                                           FieldType.Qty,
                                                                           FieldType.PriceOffset,
                                                                           FieldType.Float,
                                                                           FieldType.Percentage
                                                                       };

        private static readonly HashSet<FieldType> IntFields = new HashSet<FieldType>
                                                                   {
                                                                       FieldType.Int,
                                                                       FieldType.NumInGroup,
                                                                       FieldType.Length,
                                                                       FieldType.SeqNum
                                                                   };

        private readonly Dictionary<FieldType, CustomFieldParser> customFieldMapping;

        public Parser(SpecificationData specificationData, IDictionary<FieldType, CustomFieldParser> customFieldMapping)
        {
            if (specificationData == null)
            {
                throw new ArgumentNullException(nameof(specificationData));
            }

            this.customFieldMapping = new Dictionary<FieldType, CustomFieldParser>(customFieldMapping);
            this.specificationData = specificationData;
        }

        /// <summary>
        /// Returns a typed version of the untyped field, based on the information from <see cref="SpecificationData"/>.
        /// </summary>
        /// <remarks>
        /// If the field data, including its type, is not found in <see cref="SpecificationData"/>, an instance of <see cref="Field.String"/> is returned. 
        /// It means that all fields missing their type mappings in the spec files will be returned as 'weakly-typed' string fields.
        /// </remarks>
        /// <param name="untypedField">Untyped FIX field</param>
        /// <exception cref="NotSupportedException">Thrown when the value of <see cref="SpecField.FieldType"/> in <see cref="SpecField"/> that matches the tag of <paramref name="untypedField"/> falls out of supported range.</exception>
        /// <returns>An appropriate instance of <see cref="Field"/></returns>
        public Field MakeTyped(UntypedField untypedField)
        {
            SpecField field;
            if (!specificationData.Fields.TryGetValue(untypedField.Tag, out field))
            {
                log.TraceEvent(
                    TraceEventType.Verbose,
                    501,
                    "Could not find the specification data for field with tag {0}, assuming a String field.",
                    untypedField.Tag);
                return new Field.String(untypedField.Tag, untypedField.Value);
            }

            var tpe = field.FieldType;

            CustomFieldParser customParser;
            if (customFieldMapping.TryGetValue(tpe, out customParser))
            {
                return customParser(untypedField.Tag, untypedField.Value);
            }

            if (StringFields.Contains(tpe))
            {
                return new Field.String(untypedField.Tag, untypedField.Value);
            }
            if (DecimalFields.Contains(tpe))
            {
                return Field.Decimal.Parse(untypedField.Tag, untypedField.Value);
            }
            if (IntFields.Contains(tpe))
            {
                return Field.Int.Parse(untypedField.Tag, untypedField.Value);
            }
            if (tpe == FieldType.Boolean)
            {
                return Field.Boolean.Parse(untypedField.Tag, untypedField.Value);
            }
            if (tpe == FieldType.Char)
            {
                return Field.Char.Parse(untypedField.Tag, untypedField.Value);
            }
            if (tpe == FieldType.UtcDateOnly || tpe == FieldType.UtcDate)
            {
                return Field.Date.Parse(untypedField.Tag, untypedField.Value);
            }
            if (tpe == FieldType.UtcTimestamp || tpe == FieldType.TzTimestamp)
            {
                return Field.DateTime.Parse(untypedField.Tag, untypedField.Value);
            }
            if (tpe == FieldType.Time || tpe == FieldType.TzTimeOnly || tpe == FieldType.UtcTimeOnly)
            {
                return Field.Time.Parse(untypedField.Tag, untypedField.Value);
            }

            throw new NotSupportedException($"Unsupported FIX type: {field.FieldType}");
        }

        /// <summary>
        /// Extracts a single field from the text, starting at given position.
        /// </summary>
        /// <param name="text">Text to extract the field from</param>
        /// <param name="startPosition">Scan starting position</param>
        /// <returns>An instance of <see cref="FieldExtractionResult"/></returns>
        private static FieldExtractionResult ExtractField(string text, int startPosition)
        {
            var tagEndIndex = text.IndexOf('=', startPosition);
            if (tagEndIndex < 0)
            {
                return FieldExtractionResult.Unavailable;
            }

            var tag = int.Parse(text.Substring(startPosition, tagEndIndex - startPosition));
            startPosition = tagEndIndex + 1;
            var fieldEndIndex = text.IndexOf(Delimiters.Ascii.Soh, startPosition);

            if (fieldEndIndex < 0)
            {
                return FieldExtractionResult.Unavailable;
            }

            var value = text.Substring(startPosition, fieldEndIndex - startPosition);

            return new FieldExtractionResult(new UntypedField(tag, value), fieldEndIndex + 1);
        }

        /// <summary>
        /// Extracts a list of untyped fields from raw text.
        /// </summary>
        /// <param name="text">FIX message text</param>
        /// <returns>An instance of <see cref="List{T}"/> of <see cref="UntypedField"/></returns>
        public static IEnumerable<UntypedField> ExtractAllFields(string text)
        {
            var position = 0;
            while (position < text.Length)
            {
                var extraction = ExtractField(text, position);
                if (extraction == FieldExtractionResult.Unavailable)
                {
                    yield break;
                }

                position = extraction.EndIndex;
                yield return extraction.RawField;
            }
        }
    }
}
