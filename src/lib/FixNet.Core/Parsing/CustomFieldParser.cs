namespace FixNet.Core.Parsing
{
    using Fields;

    public delegate Field CustomFieldParser(int tag, string rawValue);
}