﻿namespace FixNet.Core.Messages
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    using Exceptions;

    using Fields;

    /// <summary>
    /// Represents a container of fields
    /// </summary>
    public abstract class FieldContainer
    {
        /// <summary>
        /// A dictionary of fields
        /// </summary>
        private readonly Dictionary<int, Field> fields = new Dictionary<int, Field>(8);
        
        private readonly Dictionary<int, Group> groups = new Dictionary<int, Group>();

        /// <summary>
        /// Removes a field with the specified tag from the container.
        /// </summary>
        /// <param name="tag">Tag of the field to remove</param>
        /// <returns><c>true</c> if the field was removed, <c>false</c> otherwise</returns>
        public bool Remove(int tag)
        {
            return fields.Remove(tag);
        }

        protected FieldContainer(FieldContainer fieldContainer, CopyType copyType)
        {
            switch (copyType)
            {
                case CopyType.Shallow:
                    fields = fieldContainer.fields;
                    groups = fieldContainer.groups;
                    break;
                case CopyType.Deep:
                    foreach (var item in fieldContainer.fields)
                    {
                        fields[item.Key] = item.Value;
                    }
                    foreach (var item in fieldContainer.groups)
                    {
                        var group = new Group(item.Value);
                        groups[item.Key] = group;
                    }
                    break;
            }
        }

        protected FieldContainer()
        {
        }

        /// <summary>
        /// Returns a collection of weakly-typed groups for a given <paramref name="counterTag"/>
        /// </summary>
        /// <param name="counterTag">Tag for the 'counter' field for the group</param>
        /// <returns><see cref="ICollection{T}"/> of groups</returns>
        public Group GetGroup(int counterTag)
        {
            Group grp;
            if (groups.TryGetValue(counterTag, out grp))
            {
                return grp;
            }

            grp = new Group(new Field.Int(counterTag, 0));
            groups[counterTag] = grp;
            return grp;
        }

        /// <summary>
        /// Adds or updates the field in the container.
        /// </summary>
        /// <param name="field">Field to add or update</param>
        /// <exception cref="ArgumentNullException">Thrown when the <paramref name="field"/> is <c>null</c></exception>
        public void Set(Field field)
        {
            if (field == null)
            {
                throw new ArgumentNullException(nameof(field));
            }

            fields[field.Tag] = field;
        }

        /// <summary>
        /// Gets a <typeparamref name="TField"/> that corresponds to given <paramref name="tag"/>, or <c>null</c> if the tag is not found.
        /// </summary>
        /// <typeparam name="TField">Field type</typeparam>
        /// <param name="tag">Tag of the field to look up</param>
        /// <exception cref="InvalidFieldTypeException">Thrown when the specified field type does not match the actual field type</exception>
        /// <returns>An instance of <typeparamref name="TField"/> or <c>null</c> if the tag is not found.</returns>
        public TField Get<TField>(int tag) where TField : Field
        {
            var rawField = Get(tag);
            if (rawField == null)
            {
                return null;
            }

            var typedField = rawField as TField;
            if (typedField == null)
            {
                throw new InvalidFieldTypeException(rawField.GetType(), typeof(TField));
            }

            return typedField;
        }

        /// <summary>
        /// Gets a <see cref="Field"/> that corresponds to given <paramref name="tag"/>, or <c>null</c> if the tag is not found.
        /// </summary>
        /// <param name="tag">Tag of the field to look up</param>
        /// <returns>An instance of <see cref="Field"/> or <c>null</c> if the tag is missing</returns>
        private Field Get(int tag)
        {
            Field field;
            return fields.TryGetValue(tag, out field) ? field : null;
        }

        /// <summary>
        /// Gets the array of tags describing the desired field order
        /// </summary>
        protected internal virtual int[] OrderedFieldTags { get; } = new int[0];

        /// <summary>
        /// Builds a string representation of this object using specified <see cref="StringBuilder"/> as a buffer.
        /// </summary>
        /// <param name="stringBuilder">A string builder</param>
        protected internal void BuildString(StringBuilder stringBuilder)
        {
            foreach (var tag in OrderedFieldTags)
            {
                var field = Get(tag);
                if (field != null)
                {
                    stringBuilder.Append(field).Append(Delimiters.Ascii.Soh);
                }
            }

            foreach (var field in fields)
            {
                if (OrderedFieldTags.Length > 0 && Array.IndexOf(OrderedFieldTags, field.Key) != -1)
                {
                    continue;
                }
                stringBuilder.Append(field).Append(Delimiters.Ascii.Soh);
            }
        }

        /// <summary>
        /// Determines whether the field container contains a field with a specific tag.
        /// </summary>
        /// <param name="tag">Tag to check</param>
        /// <returns><c>true</c> if tag is in the container, <c>false</c> otherwise</returns>
        public bool Contains(int tag)
        {
            return fields.ContainsKey(tag);
        }

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            var builder = new StringBuilder(128);
            BuildString(builder);
            foreach (var kvp in groups)
            {
                foreach (var element in kvp.Value)
                {
                    element.BuildString(builder);
                }
            }
            return builder.ToString();
        }
    }
}
