﻿namespace FixNet.Core.Messages
{
    using System.Text;

    /// <summary>
    /// Base FIX message representation
    /// </summary>
    public class Message : FieldContainer
    {
        public Message()
        {
            Header = new Header();
            Trailer = new Trailer();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Message"/> class, optionally copying specified <paramref name="header"/> or <paramref name="trailer"/>.
        /// </summary>
        /// <param name="header">Optional header to deep copy</param>
        /// <param name="trailer">Optional trailer to deep copy</param>
        /// <param name="copyType">Type of the copy to perform</param>
        public Message(Header header = null, Trailer trailer = null, CopyType copyType = CopyType.Deep)
        {
            Header = header != null ? new Header(header, copyType) : new Header();
            Trailer = trailer != null ? new Trailer(trailer, copyType) : new Trailer();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Message"/> class, copying specified <see cref="message"/> data.
        /// </summary>
        /// <param name="message">Message data to copy</param>
        /// <param name="copyType">Type of the copy to perform</param>
        public Message(Message message, CopyType copyType = CopyType.Deep) : base(message, copyType)
        {
            Header = new Header(message.Header, copyType);
            Trailer = new Trailer(message.Trailer, copyType);
        }

        /// <summary>
        /// Gets the standard message header
        /// </summary>
        public Header Header { get; }

        /// <summary>
        /// Gets the standard message trailer
        /// </summary>
        public Trailer Trailer { get; }

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            var builder = new StringBuilder(128);

            Header.BuildString(builder);
            BuildString(builder);
            Trailer.BuildString(builder);

            return builder.ToString();
        }
    }
}
