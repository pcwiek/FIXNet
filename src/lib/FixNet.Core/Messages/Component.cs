﻿namespace FixNet.Core.Messages
{
    /// <summary>
    /// Base class for embedded components.
    /// </summary>
    public abstract class Component
    {
        /// <summary>
        /// Gets the <see cref="FieldContainer"/> to which the property access should be delegated.
        /// </summary>
        protected FieldContainer Target { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Component"/> class, using specified container as an access delegation target.
        /// </summary>
        /// <param name="target">Field container that the property access should be delegated to</param>
        protected Component(FieldContainer target)
        {
            Target = target;
        }
    }
}
