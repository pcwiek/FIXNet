namespace FixNet.Core.Messages
{
    /// <summary>
    /// Represents a collection of fields that compose a group item. See also: <seealso cref="Group"/>
    /// </summary>
    public class GroupItem : FieldContainer
    {
        public GroupItem(GroupItem groupItem, CopyType copyType = CopyType.Deep) : base(groupItem, copyType)
        {
        }

        public GroupItem()
        {
        }
    }
}