﻿namespace FixNet.Core.Messages
{
    using System.Collections;
    using System.Collections.Generic;
    using Fields;

    /// <summary>
    /// Represents a weakly-typed FIX group
    /// </summary>
    public class Group : ICollection<GroupItem>
    {
        private readonly List<GroupItem> elements;
        private readonly int countFieldTag;

        public Group(Field.Int countField)
        {
            countFieldTag = countField.Tag;
            elements = new List<GroupItem>(countField.Value);
        }

        public Group(Group group, CopyType copyType = CopyType.Deep)
        {
            var field = group.CountField;
            countFieldTag = field.Tag;
            switch (copyType)
            {
                case CopyType.Shallow:
                    elements = group.elements;
                    break;
                case CopyType.Deep:
                    elements = new List<GroupItem>(field.Value);
                    foreach (var element in group.elements)
                    {
                        elements.Add(new GroupItem(element));
                    }
                    break;
            }
        }

        public IEnumerator<GroupItem> GetEnumerator()
        {
            return elements.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable) elements).GetEnumerator();
        }

        public void Add(GroupItem item)
        {
            elements.Add(item);
        }

        public void Clear()
        {
            elements.Clear();
        }

        public Field.Int CountField => new Field.Int(countFieldTag, Count);

        public bool Contains(GroupItem item)
        {
            return elements.Contains(item);
        }

        public void CopyTo(GroupItem[] array, int arrayIndex)
        {
            elements.CopyTo(array, arrayIndex);
        }

        public bool Remove(GroupItem item)
        {
            return elements.Remove(item);
        }

        public int Count => elements.Count;

        public bool IsReadOnly => ((ICollection<GroupItem>) elements).IsReadOnly;
    }
}