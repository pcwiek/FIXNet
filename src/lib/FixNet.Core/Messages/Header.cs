namespace FixNet.Core.Messages
{
    using Fields;

    /// <summary>
    /// Standard header
    /// </summary>
    public sealed class Header : FieldContainer
    {
        protected internal override int[] OrderedFieldTags { get; } = { BeginString.Tag, BodyLength.Tag, MsgType.Tag };

        /// <summary>
        /// Initializes a new instance of the <see cref="Header"/> class.
        /// </summary>
        public Header()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Header"/> class, copying specified <paramref name="header"/> data.
        /// </summary>
        /// <param name="header">Header data to copy</param>
        /// <param name="copyType">Type of the copy to perform</param>
        public Header(Header header, CopyType copyType = CopyType.Deep) : base(header, copyType)
        {
        }

        public BeginString BeginString
        {
            get
            {
                return Get<BeginString>(BeginString.Tag);
            }
            set
            {
                Set(value);
            }
        }

        public BodyLength BodyLength
        {
            get
            {
                return Get<BodyLength>(BodyLength.Tag);
            }
            set
            {
                Set(value);
            }
        }

        public MsgType MsgType
        {
            get
            {
                return Get<MsgType>(MsgType.Tag);
            }
            set
            {
                Set(value);
            }
        }

        internal SessionId ExtractSessionId()
        {
            return new SessionId(
                BeginString.Value,
                Get<SenderCompId>(SenderCompId.Tag)?.Value,
                Get<SenderSubId>(SenderSubId.Tag)?.Value,
                Get<SenderLocationId>(SenderLocationId.Tag)?.Value,
                Get<TargetCompId>(TargetCompId.Tag)?.Value,
                Get<TargetSubId>(TargetSubId.Tag)?.Value,
                Get<TargetLocationId>(TargetLocationId.Tag)?.Value,
                null);
        }

        internal void SetSessionData(SessionId sessionId)
        {
            Set(new BeginString(sessionId.BeginString));
            Set(new SenderCompId(sessionId.SenderCompId));
            Set(new TargetCompId(sessionId.TargetCompId));
            if (sessionId.SenderSubId != null)
            {
                Set(new SenderSubId(sessionId.SenderSubId));
            }
            if (sessionId.SenderLocationId != null)
            {
                Set(new SenderLocationId(sessionId.SenderLocationId));
            }
            if (sessionId.TargetSubId != null)
            {
                Set(new TargetSubId(sessionId.TargetSubId));
            }
            if (sessionId.TargetLocationId != null)
            {
                Set(new TargetLocationId(sessionId.TargetLocationId));
            }
        }
    }
}