﻿namespace FixNet.Core.Messages
{
    /// <summary>
    /// Represents a 'type' of a copy used during message processing.
    /// </summary>
    /// <remarks>
    /// Most of the copying elements deriving from <see cref="FieldContainer"/> use the <see cref="Deep"/> copy by default.
    /// If there's a guarantee that the source <see cref="FieldContainer"/> won't be mutated after creating a new type based on it, or if the consequences
    /// of shared and unexpected mutability are acceptable, then there is a <see cref="Shallow"/> option available, which will simply copy references to underlying collections trading safety for performance.
    /// </remarks>
    public enum CopyType
    {
        Shallow,
        Deep
    }
}