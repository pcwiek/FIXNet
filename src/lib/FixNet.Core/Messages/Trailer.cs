﻿namespace FixNet.Core.Messages
{
    using Fields;

    /// <summary>
    /// Standard trailer
    /// </summary>
    public sealed class Trailer : FieldContainer
    {
        protected internal override int[] OrderedFieldTags { get; } = { CheckSum.Tag };

        /// <summary>
        /// Initializes a new instance of the <see cref="Trailer"/> class.
        /// </summary>
        public Trailer()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Trailer"/> class, copying specified <paramref name="trailer"/> data.
        /// </summary>
        /// <param name="trailer">Trailer data to copy</param>
        /// <param name="copyType">Type of the copy to perform</param>
        public Trailer(Trailer trailer, CopyType copyType = CopyType.Deep) : base(trailer, copyType)
        {
        }

        public CheckSum CheckSum
        {
            get
            {
                return Get<CheckSum>(CheckSum.Tag);
            }
            set
            {
                Set(value);
            }
        }
    }
}