﻿namespace FixNet.Core.Transport.Tcp
{
    using System;
    using System.Diagnostics;
    using System.Net;
    using System.Net.Sockets;
    using System.Threading;
    using System.Threading.Tasks;

    public class TcpInitiator : IInitiator
    {
        private readonly TraceSource log = new TraceSource(nameof(TcpInitiator));

        private readonly IPEndPoint targetEndpoint;

        private readonly TcpClient client;

        private NetworkStream stream;

        public TcpInitiator(IPEndPoint targetEndpoint)
        {
            this.targetEndpoint = targetEndpoint;
            client = new TcpClient { NoDelay = true };
            log.TraceEvent(TraceEventType.Verbose, 2001, "Created a TCP client for initiator on local endpoint {0}",
                client.Client.LocalEndPoint);
        }

        public TcpInitiator(IPEndPoint localEndpoint, IPEndPoint targetEndpoint)
        {
            this.targetEndpoint = targetEndpoint;
            client = new TcpClient(localEndpoint) { NoDelay = true };
            log.TraceEvent(TraceEventType.Verbose, 2001, "Created a TCP client for initiator on local endpoint {0}", localEndpoint);
        }

        public async Task WriteAsync(byte[] data, CancellationToken cancellationToken)
        {
            try
            {
                await stream.WriteAsync(data, 0, data.Length, cancellationToken).ConfigureAwait(false);
                await stream.FlushAsync(cancellationToken).ConfigureAwait(false);
            }
            catch (ObjectDisposedException)
            {
                if (!cancellationToken.IsCancellationRequested)
                {
                    throw;
                }
            }
        }

        public async Task<DataRead> ReadAsync(CancellationToken cancellationToken)
        {
            var buffer = new byte[1024];
            try
            {
                var bytesRead =
                    await stream.ReadAsync(buffer, 0, buffer.Length, cancellationToken).ConfigureAwait(false);
                return new DataRead(bytesRead, buffer);
            }
            catch (ObjectDisposedException)
            {
                if (!cancellationToken.IsCancellationRequested)
                {
                    throw;
                }

                return new DataRead(0, buffer);
            }
        }

        public void Dispose()
        {
            client.Dispose();
        }

        public async Task StartAsync()
        {
            log.TraceInformation("Starting the TCP initiator transport, connecting to {0}:{1}", targetEndpoint.Address, targetEndpoint.Port);
            await client.ConnectAsync(targetEndpoint.Address, targetEndpoint.Port).ConfigureAwait(false);
            stream = client.GetStream();
            stream.ReadTimeout = (int)TimeSpan.FromSeconds(1).TotalMilliseconds;
        }

        public Task StopAsync()
        {
            log.TraceInformation("Stopping the TCP initiator connection to {0}:{1}", targetEndpoint.Address, targetEndpoint.Port);
            client.Close();
            return Task.CompletedTask;
        }
    }
}
