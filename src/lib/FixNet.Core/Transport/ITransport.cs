﻿namespace FixNet.Core.Transport
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;

    public struct DataRead
    {
        public DataRead(int bytesRead, byte[] buffer)
        {
            BytesRead = bytesRead;
            Buffer = buffer;
        }

        public int BytesRead { get; }
        public byte[] Buffer { get; }
    }

    public interface IInitiator : ITransport
    {
        Task StartAsync();
    }

    public interface IAcceptor : ITransport
    {
    }

    public interface ITransport : IDisposable
    {
        Task<DataRead> ReadAsync(CancellationToken cancellationToken);

        Task WriteAsync(byte[] data, CancellationToken cancellationToken);

        Task StopAsync();
    }
}
