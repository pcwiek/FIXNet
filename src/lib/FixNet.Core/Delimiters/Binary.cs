﻿namespace FixNet.Core.Delimiters
{
    using System.Text;

    public static class Binary
    {
        public const byte Soh = (byte)Ascii.Soh;

        public static readonly byte[] Checksum = Encoding.UTF8.GetBytes($"{Ascii.Soh}10=");

        public static readonly byte[] Length = Encoding.UTF8.GetBytes($"{Ascii.Soh}9=");

        public const byte ZeroAscii = (byte)'0';
    }
}
