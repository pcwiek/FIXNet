﻿namespace FixNet.Core.Extensions
{
    using System;

    /// <summary>
    /// Holds extension methods for arrays.
    /// </summary>
    internal static class ArrayExtensions
    {
        /// <summary>
        /// Returns the index of first element of the matching window inside an array, starting at specified offset.
        /// </summary>
        /// <typeparam name="T">Type of the array</typeparam>
        /// <param name="array">Source array</param>
        /// <param name="window">Window to match</param>
        /// <param name="offset">Offset at which the matching should start</param>
        /// <returns>An index of first matching element of the full window, or -1 if the window is not matched</returns>
        public static int IndexOfWindow<T>(this T[] array, T[] window, int offset) where T : IEquatable<T>
        {
            var windowWidth = window.Length;
            for (var i = offset; i < array.Length - windowWidth; i++)
            {
                var fullMatch = false;
                for (var j = 0; j < windowWidth; j++)
                {
                    if (!array[i + j].Equals(window[j]))
                    {
                        goto SkipMatch;
                    }
                }
                fullMatch = true;
                SkipMatch:
                if (fullMatch)
                {
                    return i + windowWidth;
                }
            }

            return -1;

        }

        /// <summary>
        /// Returns the index of first element of the matching window inside an array.
        /// </summary>
        /// <typeparam name="T">Type of the array</typeparam>
        /// <param name="array">Source array</param>
        /// <param name="window">Window to match</param>
        /// <returns>An index of first matching element of the full window, or -1 if the window is not matched</returns>
        public static int IndexOfWindow<T>(this T[] array, T[] window) where T : IEquatable<T>
        {
            return IndexOfWindow(array, window, 0);
        }

        /// <summary>
        /// Checks whether an array contains an item using default comparison for specified type.
        /// </summary>
        /// <typeparam name="T">Type of the array</typeparam>
        /// <param name="array">Source array</param>
        /// <param name="item">Item to check for existence</param>
        /// <returns><c>true</c> if the array holds a matching item, <c>false</c> otherwise</returns>
        public static bool Contains<T>(this T[] array, T item) where T : IEquatable<T>
        {
            for (var i = 0; i < array.Length; i++)
            {
                if (array[i].Equals(item))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
