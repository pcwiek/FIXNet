namespace FixNet.Core.Fields
{
    public sealed class TargetSubId : Field.String
    {
        public new const int Tag = 57;

        public TargetSubId(string value) : base(Tag, value)
        {
        }
    }
}