namespace FixNet.Core.Fields
{
    public sealed class SenderCompId : Field.String
    {
        public new const int Tag = 49;

        public SenderCompId(string value) : base(Tag, value)
        {
        }
    }
}