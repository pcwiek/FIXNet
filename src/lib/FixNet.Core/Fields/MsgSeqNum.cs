namespace FixNet.Core.Fields
{
    public class MsgSeqNum : Field.String
    {
        public new const int Tag = 35;

        public MsgSeqNum(string value) : base(Tag, value)
        {
        }
    }
}