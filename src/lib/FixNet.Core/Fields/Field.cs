﻿namespace FixNet.Core.Fields
{
    using System;
    using System.Globalization;
    using System.Text;

    /// <summary>
    /// Represents a basic FIX field
    /// </summary>
    public abstract class Field : IEquatable<Field>
    {
        /// <summary>Determines whether the specified object is equal to the current object.</summary>
        /// <returns>true if the specified object  is equal to the current object; otherwise, false.</returns>
        /// <param name="obj">The object to compare with the current object. </param>
        public abstract override bool Equals(object obj);

        /// <summary>Serves as the default hash function. </summary>
        /// <returns>A hash code for the current object.</returns>
        public abstract override int GetHashCode();

        public static bool operator ==(Field left, Field right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Field left, Field right)
        {
            return !Equals(left, right);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Field"/> class using specified tag and associated, stringified value.
        /// </summary>
        /// <param name="tag">Tag value</param>
        /// <param name="stringifiedValue">Stringified field value</param>
        protected Field(int tag, string stringifiedValue)
        {
            Tag = tag;
            Stringified = tag + "=" + stringifiedValue;
            var bytes = Encoding.UTF8.GetBytes(Stringified);
            Length = bytes.Length + 1;
            int acc = 0;
            foreach (var b in bytes)
            {
                acc += b;
            }
            Checksum = acc;
        }

        /// <summary>
        /// Gets the field tag number.
        /// </summary>
        public int Tag { get; }

        /// <summary>Indicates whether the current object is equal to another object of the same type.</summary>
        /// <returns>true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        /// <param name="other">An object to compare with this object.</param>
        public abstract bool Equals(Field other);

        /// <summary>Returns a FIX string that represents the current object.</summary>
        /// <returns>A FIX tag-value string that represents the current object.</returns>
        public sealed override string ToString() => Stringified;

        /// <summary>
        /// Gets the stringified value for the tag-value FIX pair
        /// </summary>
        protected string Stringified { get; }

        /// <summary>
        /// Gets the field checksum
        /// </summary>
        public int Checksum { get; }

        /// <summary>
        /// Gets the length of the formatted field in bytes
        /// </summary>
        public int Length { get; }

        /// <summary>
        /// Represents a FIX string field
        /// </summary>
        public class String : Field<string>
        {
            /// <summary>
            /// Creates a new instance of the <see cref="Field.String"/> class using specified tag and value.
            /// </summary>
            /// <param name="tag">Tag number</param>
            /// <param name="value">Value of the field</param>
            public String(int tag, string value)
                : base(tag, value, value)
            {
                if (value == null)
                {
                    throw new ArgumentNullException(nameof(value));
                }
            }
        }

        /// <summary>
        /// Represents a FIX boolean field
        /// </summary>
        public class Boolean : Field<bool>
        {
            /// <summary>
            /// Creates a new instance of the <see cref="Field.Boolean"/> class using specified tag and value.
            /// </summary>
            /// <param name="tag">Tag number</param>
            /// <param name="value">Value of the field</param>
            public Boolean(int tag, bool value)
                : base(tag, value, value ? "Y" : "N")
            {
            }

            /// <summary>
            /// Parses a tag with a given string value into a <see cref="Field.Boolean"/> field.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value. Expected values include "Y" and "N".</param>
            /// <exception cref="ArgumentOutOfRangeException">Thrown when the <paramref name="value"/> does not fall in expected range.</exception>
            /// <returns>An instance of <see cref="Field.Boolean"/></returns>
            public static Boolean Parse(int tag, string value)
            {
                Boolean field;
                if (TryParse(tag, value, out field))
                {
                    return field;
                }

                throw new ArgumentOutOfRangeException(nameof(value), $"Invalid boolean value: {value}");
            }

            /// <summary>
            /// Attempts to parse a tag with a given string value into a <see cref="Field.Boolean"/> field. A return value indicates whether the parsing succeeded.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value. Expected values include "Y" and "N".</param>
            /// <param name="field">When the method returns, this parameter will hold a value of a parsed field if the method succeeds, or a default value for <see cref="Field.Boolean"/> if the method fails.</param>
            /// <returns><c>true</c> if the parsing attempt was successful, <c>false</c> otherwise.</returns>
            public static bool TryParse(int tag, string value, out Boolean field)
            {
                field = default(Boolean);
                switch (value.ToUpperInvariant())
                {
                    case "Y":
                        field = new Boolean(tag, true);
                        return true;
                    case "N":
                        field = new Boolean(tag, false);
                        return true;
                    default:
                        return false;
                }
            }
        }

        /// <summary>
        /// Represents a FIX integer field
        /// </summary>
        public class Int : Field<int>
        {
            /// <summary>
            /// Creates a new instance of the <see cref="Field.Int"/> class using specified tag and value.
            /// </summary>
            /// <param name="tag">Tag number</param>
            /// <param name="value">Value of the field</param>
            public Int(int tag, int value)
                : base(tag, value, value.ToString())
            {
            }

            /// <summary>
            /// Parses a tag with a given string value into a <see cref="Field.Int"/> field.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value</param>
            /// <exception cref="FormatException">Thrown when the <paramref name="value"/> does not match expected format.</exception>
            /// <returns>An instance of <see cref="Field.Int"/></returns>
            public static Int Parse(int tag, string value)
            {
                Int field;
                if (!TryParse(tag, value, out field))
                {
                    throw new FormatException($"Invalid format for integer value: {value}");
                }

                return field;
            }

            /// <summary>
            /// Attempts to parse a tag with a given string value into a <see cref="Field.Int"/> field. A return value indicates whether the parsing succeeded.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value.</param>
            /// <param name="field">When the method returns, this parameter will hold a value of a parsed field if the method succeeds, or a default value for <see cref="Field.Int"/> if the method fails.</param>
            /// <returns><c>true</c> if the parsing attempt was successful, <c>false</c> otherwise.</returns>
            public static bool TryParse(int tag, string value, out Int field)
            {
                field = default(Int);
                int numericValue;
                if (!int.TryParse(value, out numericValue))
                {
                    return false;
                }

                field = new Int(tag, numericValue);
                return true;
            }
        }

        /// <summary>
        /// Represents a FIX decimal field
        /// </summary>
        public class Decimal : Field<decimal>
        {
            /// <summary>
            /// Creates a new instance of the <see cref="Field.Decimal"/> class using specified tag and value.
            /// </summary>
            /// <param name="tag">Tag number</param>
            /// <param name="value">Value of the field</param>
            public Decimal(int tag, decimal value)
                : base(tag, value, value.ToString(CultureInfo.InvariantCulture))
            {
            }

            /// <summary>
            /// Parses a tag with a given string value into a <see cref="Field.Decimal"/> field.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value</param>
            /// <exception cref="FormatException">Thrown when the <paramref name="value"/> does not match expected format.</exception>
            /// <returns>An instance of <see cref="Field.Decimal"/></returns>
            public static Decimal Parse(int tag, string value)
            {
                Decimal field;
                if (!TryParse(tag, value, out field))
                {
                    throw new FormatException($"Invalid format for decimal value: {value}");
                }

                return field;
            }

            /// <summary>
            /// Attempts to parse a tag with a given string value into a <see cref="Field.Decimal"/> field. A return value indicates whether the parsing succeeded.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value.</param>
            /// <param name="field">When the method returns, this parameter will hold a value of a parsed field if the method succeeds, or a default value for <see cref="Field.Decimal"/> if the method fails.</param>
            /// <returns><c>true</c> if the parsing attempt was successful, <c>false</c> otherwise.</returns>
            public static bool TryParse(int tag, string value, out Decimal field)
            {
                field = default(Decimal);
                decimal numericValue;
                if (!decimal.TryParse(value, NumberStyles.Number, CultureInfo.InvariantCulture, out numericValue))
                {
                    return false;
                }

                field = new Decimal(tag, numericValue);
                return true;
            }
        }

        /// <summary>
        /// Represents a FIX character field
        /// </summary>
        public class Char : Field<char>
        {
            /// <summary>
            /// Creates a new instance of the <see cref="Field.Char"/> class using specified tag and value.
            /// </summary>
            /// <param name="tag">Tag number</param>
            /// <param name="value">Value of the field</param>
            public Char(int tag, char value)
                : base(tag, value, value.ToString())
            {
            }

            /// <summary>
            /// Parses a tag with a given string value into a <see cref="Field.Char"/> field.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value</param>
            /// <exception cref="ArgumentException">Thrown when trying to parse a multi-character <paramref name="value"/> as a character.</exception>
            /// <returns>An instance of <see cref="Field.Char"/></returns>
            public static Char Parse(int tag, string value)
            {
                Char field;
                if(!TryParse(tag, value, out field))
                {
                    throw new ArgumentException(
                        $"Trying to pass multi-character string as a character value: {value}",
                        nameof(value));
                }

                return field;
            }

            /// <summary>
            /// Attempts to parse a tag with a given string value into a <see cref="Field.Char"/> field. A return value indicates whether the parsing succeeded.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value. Expected value is a one-character string.</param>
            /// <param name="field">When the method returns, this parameter will hold a value of a parsed field if the method succeeds, or a default value for <see cref="Field.Char"/> if the method fails.</param>
            /// <returns><c>true</c> if the parsing attempt was successful, <c>false</c> otherwise.</returns>
            public static bool TryParse(int tag, string value, out Char field)
            {
                field = default(Char);

                if (value.Length > 1)
                {
                    return false;
                }

                field = new Char(tag, value[0]);

                return true;
            }
        }

        /// <summary>
        /// Represents a FIX datetime field
        /// </summary>
        public class DateTime : Field<System.DateTime>
        {
            private const string FormatWithMilliseconds = "yyyyMMdd-HH:mm:ss.fff";

            private const string FormatWithoutMilliseconds = "yyyyMMdd-HH:mm:ss";

            private static readonly string[] Formats = { FormatWithoutMilliseconds, FormatWithMilliseconds };

            /// <summary>
            /// Initializes a new instance of the <see cref="Field.DateTime"/> class using specified tag, value and a flag indicating whether the field should include milliseconds.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual field value</param>
            /// <param name="includeMilliseconds">Indicates whether the serialized field should include milliseconds or not</param>
            public DateTime(int tag, System.DateTime value, bool includeMilliseconds)
                : base(
                    tag,
                    value,
                    includeMilliseconds
                        ? value.ToString(FormatWithMilliseconds)
                        : value.ToString(FormatWithoutMilliseconds))
            {
            }

            /// <summary>
            /// Parses a tag with a given string value into a <see cref="Field.DateTime"/> field.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value</param>
            /// <param name="includeMilliseconds">A flag indicating whether the serialized form should include milliseconds or not.</param>
            /// <exception cref="FormatException">Thrown when the <paramref name="value"/> does not match any of the supported formats.</exception>
            /// <returns>An instance of <see cref="Field.DateTime"/></returns>
            public static DateTime Parse(int tag, string value, bool includeMilliseconds = true)
            {
                DateTime field;
                if (!TryParse(tag, value, out field, includeMilliseconds))
                {
                    throw new FormatException(
                        $"Provided date time value does not match any of the supported formats ({FormatWithoutMilliseconds}, {FormatWithMilliseconds}): {value}");
                }

                return field;
            }

            /// <summary>
            /// Attempts to parse a tag with a given string value into a <see cref="Field.DateTime"/> field. A return value indicates whether the parsing succeeded.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value.</param>
            /// <param name="includeMilliseconds">A flag indicating whether the serialized form should include milliseconds or not.</param>
            /// <param name="field">When the method returns, this parameter will hold a value of a parsed field if the method succeeds, or a default value for <see cref="Field.DateTime"/> if the method fails.</param>
            /// <returns><c>true</c> if the parsing attempt was successful, <c>false</c> otherwise.</returns>
            public static bool TryParse(int tag, string value, out DateTime field, bool includeMilliseconds = true)
            {
                field = default(DateTime);
                System.DateTime dateTimeValue;
                if (
                    !System.DateTime.TryParseExact(
                        value,
                        Formats,
                        CultureInfo.InvariantCulture,
                        DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal,
                        out dateTimeValue))
                {
                    return false;
                }
                field = new DateTime(tag, dateTimeValue, includeMilliseconds);
                return true;
            }
        }

        /// <summary>
        /// Represents a FIX time-only field
        /// </summary>
        public class Time : Field<TimeSpan>
        {
            private const string FormatWithMilliseconds = "hh\\:mm\\:\\ss\\.fff";

            private const string FormatWithoutMilliseconds = "hh\\:mm\\:ss";

            private static readonly string[] Formats = { FormatWithoutMilliseconds, FormatWithMilliseconds };

            /// <summary>
            /// Initializes a new instnace of the <see cref="Field.Time"/> class using specified tag, value and a flag indicating whether the field should include milliseconds.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual field value</param>
            /// <param name="includeMilliseconds">Indicates whether the serialized field should include milliseconds or not</param>
            public Time(int tag, TimeSpan value, bool includeMilliseconds)
                : base(
                    tag,
                    value,
                    includeMilliseconds
                        ? value.ToString(FormatWithMilliseconds)
                        : value.ToString(FormatWithoutMilliseconds))
            {
            }

            /// <summary>
            /// Parses a tag with a given string value into a <see cref="Field.Time"/> field.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value</param>
            /// <param name="includeMilliseconds">A flag indicating whether the serialized form should include milliseconds or not.</param>
            /// <exception cref="FormatException">Thrown when the <paramref name="value"/> does not match any of the supported formats.</exception>
            /// <returns>An instance of <see cref="Field.Time"/></returns>
            public static Time Parse(int tag, string value, bool includeMilliseconds = true)
            {
                Time field;
                if (TryParse(tag, value, out field, includeMilliseconds))
                {
                    return field;
                }

                throw new FormatException(
                    $"Provided time value does not match any of the supported formats ({FormatWithoutMilliseconds}, {FormatWithMilliseconds}): {value}");
            }

            /// <summary>
            /// Attempts to parse a tag with a given string value into a <see cref="Field.Time"/> field. A return value indicates whether the parsing succeeded.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value.</param>
            /// <param name="includeMilliseconds">A flag indicating whether the serialized form should include milliseconds or not.</param>
            /// <param name="field">When the method returns, this parameter will hold a value of a parsed field if the method succeeds, or a default value for <see cref="Field.Time"/> if the method fails.</param>
            /// <returns><c>true</c> if the parsing attempt was successful, <c>false</c> otherwise.</returns>
            public static bool TryParse(int tag, string value, out Time field, bool includeMilliseconds = true)
            {
                TimeSpan timeValue;
                if (TimeSpan.TryParseExact(value, Formats, CultureInfo.InvariantCulture, out timeValue))
                {
                    field = new Time(tag, timeValue, includeMilliseconds);
                    return true;
                }

                field = default(Time);
                return false;
            }
        }

        /// <summary>
        /// Represents a FIX date-only field
        /// </summary>
        public class Date : Field<System.DateTime>
        {
            private const string Format = "yyyyMMdd";

            /// <summary>
            /// Initializes a new instnace of the <see cref="Field.Date"/> class using specified tag and value.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual field value</param>
            public Date(int tag, System.DateTime value)
                : base(tag, value, value.ToString(Format))
            {
            }

            /// <summary>
            /// Parses a tag with a given string value into a <see cref="Field.Date"/> field.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value</param>
            /// <exception cref="FormatException">Thrown when the <paramref name="value"/> does not match supported format.</exception>
            /// <returns>An instance of <see cref="Field.Date"/></returns>
            public static Date Parse(int tag, string value)
            {
                Date field;
                if (TryParse(tag, value, out field))
                {
                    return field;
                }

                throw new FormatException(
                    $"Provided date time value does not match the supported format ({Format}): {value}");
            }

            /// <summary>
            /// Attempts to parse a tag with a given string value into a <see cref="Field.Date"/> field. A return value indicates whether the parsing succeeded.
            /// </summary>
            /// <param name="tag">Field tag value</param>
            /// <param name="value">Actual, raw field value.</param>
            /// <param name="field">When the method returns, this parameter will hold a value of a parsed field if the method succeeds, or a default value for <see cref="Field.Date"/> if the method fails.</param>
            /// <returns><c>true</c> if the parsing attempt was successful, <c>false</c> otherwise.</returns>
            public static bool TryParse(int tag, string value, out Date field)
            {
                field = default(Date);
                System.DateTime dateTimeValue;
                if (
                    !System.DateTime.TryParseExact(
                        value,
                        Format,
                        CultureInfo.InvariantCulture,
                        DateTimeStyles.AssumeUniversal | DateTimeStyles.AdjustToUniversal,
                        out dateTimeValue))
                {
                    return false;
                }

                field = new Date(tag, dateTimeValue);
                return true;
            }
        }
    }

    /// <summary>
    /// Represents a FIX field with associated value
    /// </summary>
    /// <typeparam name="T">Type of the value</typeparam>
    public abstract class Field<T> : Field, IEquatable<Field<T>>
    {
        /// <summary>
        /// Gets the value of the field
        /// </summary>
        public T Value { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Field{T}"/> class using specified tag, value and stringified representation of the value.
        /// </summary>
        /// <param name="tag">Field tag value</param>
        /// <param name="value">Actual field value</param>
        /// <param name="stringifiedValue">Stringified and formatted field value</param>
        protected Field(int tag, T value, string stringifiedValue)
            : base(tag, stringifiedValue)
        {
            Value = value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Field{T}"/> class using specified tag and value.
        /// </summary>
        /// <remarks>
        /// The stringified representation of <paramref name="value"/> is obtained via <see cref="string.Format(IFormatProvider, string, object)"/> using <see cref="CultureInfo.InvariantCulture"/>
        /// </remarks>
        /// <param name="tag">Field tag value</param>
        /// <param name="value">Actual field value</param>
        protected Field(int tag, T value) : this(tag, value, string.Format(CultureInfo.InvariantCulture, "{0}", value))
        {
        }

        /// <summary>Determines whether the specified object is equal to the current object.</summary>
        /// <returns>true if the specified object  is equal to the current object; otherwise, false.</returns>
        /// <param name="obj">The object to compare with the current object. </param>
        public override bool Equals(object obj)
        {
            var field = obj as Field<T>;
            if (field == null)
            {
                return false;
            }
            return Equals(field);
        }

        /// <summary>Serves as the default hash function. </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            unchecked
            {
                return (Tag * 397) ^ Value.GetHashCode();
            }
        }

        /// <summary>Indicates whether the current object is equal to another object of the same type.</summary>
        /// <returns>true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        /// <param name="other">An object to compare with this object.</param>
        public override bool Equals(Field other)
        {
            var field = other as Field<T>;
            if (field == null)
            {
                return false;
            }
            return Equals(field);
        }

        /// <summary>Indicates whether the current object is equal to another object of the same type.</summary>
        /// <returns>true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals(Field<T> other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Tag == other.Tag && Value.Equals(other.Value);
        }
    }
}

