﻿namespace FixNet.Core.Fields
{
    public sealed class CheckSum : Field.Int
    {
        public new const int Tag = 10;

        public CheckSum(int value) : base(Tag, value)
        {
        }
    }
}