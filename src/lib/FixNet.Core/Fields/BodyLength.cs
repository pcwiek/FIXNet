namespace FixNet.Core.Fields
{
    public sealed class BodyLength : Field.Int
    {
        public new const int Tag = 9;

        public BodyLength(int value) : base(Tag, value)
        {
        }
    }
}