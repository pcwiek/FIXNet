namespace FixNet.Core.Fields
{
    public sealed class BeginString : Field.String
    {
        public new const int Tag = 8;

        public BeginString(string value) : base(Tag, value)
        {
        }
    }
}