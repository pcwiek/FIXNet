namespace FixNet.Core.Fields
{
    public sealed class TargetLocationId : Field.String
    {
        public new const int Tag = 143;

        public TargetLocationId(string value) : base(Tag, value)
        {
        }
    }
}