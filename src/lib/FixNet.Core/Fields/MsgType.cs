namespace FixNet.Core.Fields
{
    public sealed class MsgType : Field.String
    {
        public new const int Tag = 35;

        public MsgType(string value) : base(Tag, value)
        {
        }
    }
}