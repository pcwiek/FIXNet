namespace FixNet.Core.Fields
{
    public sealed class SenderSubId : Field.String
    {
        public new const int Tag = 50;

        public SenderSubId(string value) : base(Tag, value)
        {
        }
    }
}