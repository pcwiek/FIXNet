namespace FixNet.Core.Fields
{
    public sealed class SenderLocationId : Field.String
    {
        public new const int Tag = 142;

        public SenderLocationId(string value) : base(Tag, value)
        {
        }
    }
}