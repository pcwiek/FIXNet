namespace FixNet.Core.Fields
{
    public sealed class TargetCompId : Field.String
    {
        public new const int Tag = 56;

        public TargetCompId(string value) : base(Tag, value)
        {
        }
    }
}