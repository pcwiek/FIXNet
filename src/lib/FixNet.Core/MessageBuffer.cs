﻿namespace FixNet.Core
{
    using System;
    using System.Text;

    using Delimiters;

    using Extensions;

    internal sealed class MessageBuffer
    {
        private byte[] buffer;

        private int bytesUsed;

        public MessageBuffer(byte[] data)
        {
            buffer = data;
            bytesUsed = data.Length;
        }

        public MessageBuffer()
        {
            buffer = new byte[1024];
        }

        public void AddChunk(byte[] data)
        {
            var incomingLength = data.Length;
            if (incomingLength + bytesUsed > buffer.Length)
            {
                Array.Resize(ref buffer, (incomingLength + bytesUsed) * 2);
            }
            Buffer.BlockCopy(data, 0, buffer, bytesUsed, incomingLength);
            bytesUsed += incomingLength;
        }

        public string ExtractMessage()
        {
            var length = ExtractLength(buffer);
            if (length == -1)
            {
                return null;
            }

            var checksumStartIndex = buffer.IndexOfWindow(Binary.Checksum, length);

            if (checksumStartIndex == -1)
            {
                return null;
            }

            for (var i = checksumStartIndex; i < buffer.Length; i++)
            {
                if (buffer[i] != Binary.Soh)
                {
                    continue;
                }

                var data = Encoding.UTF8.GetString(buffer, 0, i + 1);
                var itemsRemaining = buffer.Length - i;
                Array.Copy(buffer, itemsRemaining, buffer, 0, buffer.Length - itemsRemaining);
                Array.Clear(buffer, itemsRemaining, buffer.Length - itemsRemaining);
                bytesUsed -= i;
                return data;
            }

            return null;
        }

        private static int ExtractLength(byte[] buffer)
        {
            int lengthEndIndex = -1;
            var length = buffer.Length;
            if (length < 3)
            {
                return -1;
            }

            int lengthStartIndex = buffer.IndexOfWindow(Binary.Length);

            if (lengthStartIndex == -1)
            {
                return -1;
            }

            for (int i = lengthStartIndex; i < length; i++)
            {
                if (buffer[i] == Binary.Soh)
                {
                    lengthEndIndex = i;
                    break;
                }
            }

            if (lengthEndIndex == -1)
            {
                return -1;
            }

            return AsciiBytesToInt(new ArraySegment<byte>(buffer, lengthStartIndex, lengthEndIndex - lengthStartIndex));
        }

        private static int AsciiBytesToInt(ArraySegment<byte> segment)
        {
            int number = 0;
            foreach (var b in segment)
            {
                var adjusted = b - Binary.ZeroAscii;
                if (adjusted < 0)
                {
                    throw new InvalidOperationException("Message length cannot be negative");
                }

                if (adjusted == 0)
                {
                    continue;
                }

                number = number * 10 + adjusted;
            }
            return number;
        }
    }
}
