﻿namespace FixNet.Core.Storage
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Allows access to the message & session store.
    /// </summary>
    public interface IMessageStore : IDisposable
    {
        /// <summary>
        /// Gets or sets the target sequence number.
        /// </summary>
        int TargetSequenceNumber { get; set; }

        /// <summary>
        /// Gets or sets the sender sequence number.
        /// </summary>
        int SenderSequenceNumber { get; set; }

        /// <summary>
        /// Gets the store creation time.
        /// </summary>
        DateTime CreationTime { get; }

        /// <summary>
        /// Retrieves a range of messages between sequence numbers, inclusive.
        /// </summary>
        /// <param name="sequenceStart">Sequence number to start with</param>
        /// <param name="sequenceEnd">Last sequence number to retrieve</param>
        /// <returns>A <see cref="IReadOnlyList{T}"/></returns>
        IReadOnlyList<RawMessage> GetRange(int sequenceStart, int sequenceEnd);

        /// <summary>
        /// Associates the sequence number with the specified raw message.
        /// </summary>
        /// <param name="seqNo">Sequence number</param>
        /// <param name="message">Raw message</param>
        void Set(int seqNo, RawMessage message);

        /// <summary>
        /// Clears the store.
        /// </summary>
        void Clear();
    }
}
