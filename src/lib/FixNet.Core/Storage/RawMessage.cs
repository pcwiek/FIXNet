namespace FixNet.Core.Storage
{
    using System;

    /// <summary>
    /// Raw message, which is used in session and message storage.
    /// </summary>
    public struct RawMessage : IEquatable<RawMessage>
    {
        public bool Equals(RawMessage other)
        {
            return string.Equals(Text, other.Text);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is RawMessage && Equals((RawMessage)obj);
        }

        public override int GetHashCode()
        {
            return Text.GetHashCode();
        }

        public static bool operator ==(RawMessage left, RawMessage right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(RawMessage left, RawMessage right)
        {
            return !left.Equals(right);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RawMessage"/> struct using specified text.
        /// </summary>
        /// <param name="text">Message text</param>
        public RawMessage(string text)
        {
            Text = text;
        }

        /// <summary>
        /// Gets the text of the message.
        /// </summary>
        public string Text { get; }
    }
}