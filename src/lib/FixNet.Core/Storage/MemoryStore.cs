namespace FixNet.Core.Storage
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// An in-memory store for messages & sessions.
    /// </summary>
    public class MemoryStore : IMessageStore
    {
        private readonly Dictionary<int, RawMessage> messages = new Dictionary<int, RawMessage>();

        public int TargetSequenceNumber { get; set; }

        public int SenderSequenceNumber { get; set; }

        public DateTime CreationTime { get; }

        public MemoryStore()
        {
            CreationTime = DateTime.UtcNow;
        }

        public IReadOnlyList<RawMessage> GetRange(int sequenceStart, int sequenceEnd)
        {
            var list = new List<RawMessage>(sequenceEnd - sequenceStart + 1);
            for (int i = sequenceStart; i <= sequenceEnd; i++)
            {
                RawMessage message;
                if (messages.TryGetValue(i, out message))
                {
                    list.Add(message);
                }
            }
            return list;
        }

        public void Set(int seqNo, RawMessage message)
        {
            messages[seqNo] = message;
        }

        public void Clear()
        {
            messages.Clear();
        }

        public void Dispose()
        {
        }
    }
}