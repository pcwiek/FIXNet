namespace FixNet.Core.Exceptions
{
    using System;

    public class MessageVerificationException : Exception
    {
        public MessageVerificationException(string message)
            : base(message)
        {
        }
        
    }

    public class VerificationPreconditionException : MessageVerificationException
    {
        public VerificationPreconditionException(string message)
            : base(message)
        {
        }
    }
}