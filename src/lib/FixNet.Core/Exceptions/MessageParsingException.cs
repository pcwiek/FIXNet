namespace FixNet.Core.Exceptions
{
    public class MessageParsingException : FixNetException
    {
        public MessageParsingException(string message)
            : base(message)
        {
        }
    }
}