namespace FixNet.Core.Exceptions
{
    using System;

    public abstract class FixNetException : Exception
    {
        public FixNetException()
        {
        }

        public FixNetException(string message)
            : base(message)
        {
        }

        public FixNetException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}