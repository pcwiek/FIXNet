﻿namespace FixNet.Core.Exceptions
{
    using System;

    public class MissingHandlerException : FixNetException
    {
        public MissingHandlerException(string messageTypeName)
            : base("No handler specified for message " + messageTypeName)
        {
        }

        public MissingHandlerException(string messageTypeName, Exception inner)
            : base("No handler specified for message " + messageTypeName, inner)
        {
        }
    }
}
