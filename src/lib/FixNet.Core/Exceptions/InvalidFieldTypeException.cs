namespace FixNet.Core.Exceptions
{
    using System;

    public class InvalidFieldTypeException : FixNetException
    {
        public Type ActualType { get; }

        public Type SpecifiedType { get; }

        public InvalidFieldTypeException(Type actualType, Type specifiedType)
            : base($"Couldn't coerce a field with type {actualType} into {specifiedType}")
        {
            ActualType = actualType;
            SpecifiedType = specifiedType;
        }
    }
}