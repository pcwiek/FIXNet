﻿namespace FixNet.Core
{
    using System;

    public sealed class SessionId : IEquatable<SessionId>
    {
        private readonly string sessionIdentifier;

        public SessionId(string beginString,
            string targetCompId, 
            string senderCompId) : this(beginString, senderCompId, null, null, targetCompId, null, null, null) { }

        public SessionId(
            string beginString, 
            string senderCompId, 
            string senderSubId, 
            string senderLocationId, 
            string targetCompId, 
            string targetSubId, 
            string targetLocationId, 
            string qualifier)
        {
            if (string.IsNullOrEmpty(beginString))
            {
                throw new ArgumentException("Value cannot be null or empty.", nameof(beginString));
            }
            if (string.IsNullOrEmpty(senderCompId))
            {
                throw new ArgumentException("Value cannot be null or empty.", nameof(senderCompId));
            }
            if (string.IsNullOrEmpty(targetCompId))
            {
                throw new ArgumentException("Value cannot be null or empty.", nameof(targetCompId));
            }

            if (senderSubId == string.Empty)
            {
                throw new ArgumentException("Empty string is not a valid value.", nameof(senderSubId));
            }
            if (targetSubId == string.Empty)
            {
                throw new ArgumentException("Empty string is not a valid value.", nameof(targetSubId));
            }
            if (senderLocationId == string.Empty)
            {
                throw new ArgumentException("Empty string is not a valid value.", nameof(senderLocationId));
            }
            if (targetLocationId == string.Empty)
            {
                throw new ArgumentException("Empty string is not a valid value.", nameof(targetLocationId));
            }

            Qualifier = qualifier;
            BeginString = beginString;
            SenderCompId = senderCompId;
            SenderSubId = senderSubId;
            SenderLocationId = senderLocationId;
            TargetCompId = targetCompId;
            TargetSubId = targetSubId;
            TargetLocationId = targetLocationId;

            sessionIdentifier = beginString + ":" + senderCompId
                                + (!string.IsNullOrEmpty(senderSubId) ? "/" + senderSubId : string.Empty)
                                + (!string.IsNullOrEmpty(senderLocationId) ? "/" + senderLocationId : string.Empty)
                                + "->" + targetCompId
                                + (!string.IsNullOrEmpty(targetSubId) ? "/" + targetSubId : string.Empty)
                                + (!string.IsNullOrEmpty(targetLocationId) ? "/" + targetLocationId : string.Empty);

            if (!string.IsNullOrEmpty(Qualifier))
            {
                sessionIdentifier += ":" + qualifier;
            }
        }

        public string BeginString { get; }

        public string Qualifier { get; }

        public string SenderCompId { get; }

        public string SenderLocationId { get; }

        public string SenderSubId { get; }

        public string TargetCompId { get; }

        public string TargetLocationId { get; }

        public string TargetSubId { get; }

        public static bool operator ==(SessionId left, SessionId right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(SessionId left, SessionId right)
        {
            return !Equals(left, right);
        }

        /// <summary>Indicates whether the current object is equal to another object of the same type.</summary>
        /// <returns>true if the current object is equal to the <paramref name="other" /> parameter; otherwise, false.</returns>
        /// <param name="other">An object to compare with this object.</param>
        public bool Equals(SessionId other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return string.Equals(sessionIdentifier, other.sessionIdentifier);
        }

        /// <summary>Determines whether the specified object is equal to the current object.</summary>
        /// <returns>true if the specified object  is equal to the current object; otherwise, false.</returns>
        /// <param name="obj">The object to compare with the current object. </param>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj is SessionId && Equals((SessionId)obj);
        }

        /// <summary>Serves as the default hash function. </summary>
        /// <returns>A hash code for the current object.</returns>
        public override int GetHashCode()
        {
            return sessionIdentifier.GetHashCode();
        }

        /// <summary>Returns a string that represents the current object.</summary>
        /// <returns>A string that represents the current object.</returns>
        public override string ToString()
        {
            return sessionIdentifier;
        }
    }
}