﻿namespace FixNet.Core
{
    using System.Collections.Generic;
    using Parsing;
    using Specification;

    public abstract partial class Session
    {
        public class Settings
        {
            public SpecificationData SpecificationData { get; }
            public MessageBuilder.VerificationOptions MessageVerificationOptions { get; }
            public IDictionary<FieldType, CustomFieldParser> CustomFieldMapping { get; }

            public SessionId SessionId { get; }

            public Settings(SpecificationData specificationData,
                MessageBuilder.VerificationOptions messageVerificationOptions,
                IDictionary<FieldType, CustomFieldParser> customFieldMapping, SessionId sessionId)
            {
                SpecificationData = specificationData;
                MessageVerificationOptions = messageVerificationOptions;
                CustomFieldMapping = customFieldMapping;
                SessionId = sessionId;
            }

            public Settings(SessionId sessionId)
                : this(
                    SpecificationData.Empty, MessageBuilder.VerificationOptions.None,
                    new Dictionary<FieldType, CustomFieldParser>(), sessionId)
            {
            }
        }
    }
}
