﻿namespace FixNet.Core
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Messages;

    /// <summary>
    /// Represents the core session functionality
    /// </summary>
    public interface ISession
    {
        /// <summary>
        /// Occurs when a new message arrives.
        /// </summary>
        event EventHandler<Message> NewMessage;

        /// <summary>
        /// Sends a message asynchronously with cancellation support.
        /// </summary>
        /// <param name="message">Message to send</param>
        /// <param name="cancellationToken">Cancellation token to cancel the sending process</param>
        /// <returns>A <see cref="Task"/></returns>
        Task SendAsync(Message message, CancellationToken cancellationToken);

        /// <summary>
        /// Sends a message asynchronously.
        /// </summary>
        /// <param name="message">Message to send</param>
        /// <returns>A <see cref="Task"/></returns>
        Task SendAsync(Message message);

        /// <summary>
        /// Stops the session asynchronously.
        /// </summary>
        /// <returns>A <see cref="Task"/></returns>
        Task StopAsync();
    }
}