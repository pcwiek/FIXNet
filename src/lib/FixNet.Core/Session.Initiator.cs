﻿namespace FixNet.Core
{
    using System;
    using System.Threading.Tasks;
    using Transport;

    public partial class Session
    {
        public class Initiator : Session
        {
            internal Initiator(State state, ITransport transport, Settings settings) : base(state, transport, settings)
            {
            }

            public static async Task<ISession> CreateAsync(IInitiator transport, Settings settings)
            {
                var initiator = new Initiator(new State(true, () => DateTime.UtcNow), transport, settings);
                await transport.StartAsync().ConfigureAwait(false);
                initiator.Start();
                return initiator;
            }
        }
    }
}
