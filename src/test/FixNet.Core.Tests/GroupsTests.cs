﻿namespace FixNet.Core.Tests
{
    using System.Linq;
    using Fields;
    using Messages;
    using NUnit.Framework;

    [TestFixture]
    public class GroupsTests
    {
        private class TestField : Field.Int
        {
            public new const int Tag = 123;

            public TestField(int value) : base(Tag, value)
            {
            }
        }

        private class OtherGroupElement : GroupItem
        {
            
        }

        private class MyGroupElement : GroupItem
        {
            public TestField MyTestField
            {
                get { return Get<TestField>(TestField.Tag); }
                set { Set(value); }
            }

            public MyGroupElement(GroupItem groupItem, CopyType copyType = CopyType.Deep) : base(groupItem, copyType)
            {
            }

            public MyGroupElement()
            {
            }
        }

        private const int MyGroupNoTag = 312;

        private class MyMessage : Message
        {
            public MyMessage()
            {
                MyElements.Add(new MyGroupElement
                {
                    MyTestField = new TestField(111)
                });
            }

            public MyMessage(Message message) : base(message) { }

            public Group MyElements => GetGroup(MyGroupNoTag);
        }

        [Test]
        public void WeaklyTypedGroupShouldBeProperlyRetrieved()
        {
            var message = new MyMessage();
            var grp = message.MyElements;
            Assert.IsNotEmpty(grp);
             
            var element = grp.Single();
            Assert.IsInstanceOf<MyGroupElement>(element);
        }


        [Test]
        public void ElementsShouldBeRemovableFromGroup()
        {
            var message = new MyMessage();
            var grp = message.MyElements;
            var element = grp.First();
            grp.Remove(element);
            Assert.IsEmpty(grp);
        }

        [Test]
        public void GroupsShouldBeProperlyHandledOnMessageDeepCopy()
        {
            var message = new MyMessage();
            var copy = new MyMessage(message);
            Assert.IsTrue(message.MyElements.Select(e => e.Get<TestField>(TestField.Tag))
                .SequenceEqual(copy.MyElements.Select(e => e.Get<TestField>(TestField.Tag))));
        }

        [Test]
        public void GroupElementsShouldBeCastable()
        {
            var message = new MyMessage();
            var copy = new MyMessage(message);
            var elements = copy.MyElements.Select(e => new MyGroupElement(e, CopyType.Shallow)).ToList();
        }
    }
}
