﻿namespace FixNet.Core.Tests
{
    using System;
    using System.Collections.Generic;

    using Fields;
    using Specification;

    using NUnit.Framework;

    [TestFixture]
    public class SpecificationDataTests
    {
        [Test]
        public void InitializingSpecDataWithNullFieldMappingShouldThrow()
        {
            Assert.Throws<ArgumentNullException>(
                () => new SpecificationData(null, new Dictionary<MsgType, SpecElement>(), null, null));
        }

        [Test]
        public void InitializingSpecDataWithNullMessageMappingShouldThrow()
        {
            Assert.Throws<ArgumentNullException>(
                () => new SpecificationData(new Dictionary<int, SpecField>(), null, null, null));
        }

        [Test]
        public void HeaderAndTrailerSpecsAreOptional()
        {
            Assert.DoesNotThrow(
                () =>
                new SpecificationData(
                    new Dictionary<int, SpecField>(),
                    new Dictionary<MsgType, SpecElement>(),
                    null,
                    null));
        }
    }
}