﻿namespace FixNet.Core.Tests
{
    using System;

    using Exceptions;

    using Fields;

    using Messages;

    using NUnit.Framework;

    [TestFixture]
    public class FieldContainerTests
    {
        private class SimpleContainer : FieldContainer
        {
            /// <summary>Returns a string that represents the current object.</summary>
            /// <returns>A string that represents the current object.</returns>
            public override string ToString()
            {
                return "Test ToString";
            }
        }

        [Test]
        public void FailureToRemoveReturnsFalse()
        {
            var container = new SimpleContainer();
            Assert.IsFalse(container.Remove(123));
        }

        [Test]
        public void ShouldBeAbleToSetField()
        {
            var container = new SimpleContainer();
            container.Set(new Field.String(111, "Abc"));
            Assert.IsTrue(container.Contains(111));
        }

        [Test]
        public void ShouldNotBeAbleToSetNullFieldValue()
        {
            var container = new SimpleContainer();
            Assert.Throws<ArgumentNullException>(() => container.Set(null));
        }

        [Test]
        public void ShouldBeAbleToGetField()
        {
            var container = new SimpleContainer();
            container.Set(new Field.String(111, "Abc"));
            var result = container.Get<Field.String>(111);
            Assert.AreEqual("Abc", result.Value);
        }

        [Test]
        public void ShouldBeAbleToGetFieldAsSupertype()
        {
            var container = new SimpleContainer();
            container.Set(new Field.String(111, "Abc"));
            var result = container.Get<Field<string>>(111);
            Assert.AreEqual("Abc", result.Value);
        }

        [Test]
        public void CoercingAFieldToWrongTypeShouldThrowException()
        {
            var container = new SimpleContainer();
            container.Set(new Field.String(111, "Abc"));
            try
            {
                var result = container.Get<Field.Int>(111);
                Assert.Fail($"Got {result}, should have failed.");
            }
            catch (InvalidFieldTypeException ex)
            {
                Assert.AreEqual(ex.ActualType, typeof(Field.String));
                Assert.AreEqual(ex.SpecifiedType, typeof(Field.Int));
            }
        }

        [Test]
        public void StringifiedContainerShouldReturnCorrectValue()
        {
            var container = new SimpleContainer();
            Assert.AreEqual("Test ToString", container.ToString());
        }
    }
}
