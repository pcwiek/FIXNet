﻿namespace FixNet.Core.Tests
{
    using System;
    using System.Text;

    using NUnit.Framework;

    [TestFixture]
    public class MessageBufferTests
    {
        [Test]
        public void MessageShouldBeCorrectlyExtractedFromString()
        {
            const string Msg =
                "8=FIX.4.49=15535=D34=21549=CLIENT4452=20060425-19:42:55.71856=TW441=juinternal11=1234621=138=140=244=554=155=EAS59=060=20060425-14:42:55207=ZZ461=ESXXXX10=013";
            var buffer = new MessageBuffer(Encoding.UTF8.GetBytes(Msg));
            var result = buffer.ExtractMessage();
            Assert.AreEqual(Msg, result);
        }

        [Test]
        public void ExceptionShouldBeThrownOnNegativeLength()
        {
            const string Msg =
                "8=FIX.4.49=-5535=D34=21549=CLIENT4452=20060425-19:42:55.71856=TW441=juinternal11=1234621=138=140=244=554=155=EAS59=060=20060425-14:42:55207=ZZ461=ESXXXX10=013";
            var buffer = new MessageBuffer(Encoding.UTF8.GetBytes(Msg));
            Assert.Throws<InvalidOperationException>(() => buffer.ExtractMessage());
        }
    }
}
