﻿namespace FixNet.Core.Tests
{
    using Fields;
    using Messages;
    using NUnit.Framework;

    [TestFixture]
    public class ComponentsTests
    {
        private class CustomField : Field<int>
        {
            public new const int Tag = 123;

            public CustomField(int value) : base(Tag, value)
            {
            }
        }

        private class TestComponent : Component
        {
            public TestComponent(FieldContainer target) : base(target)
            {
            }

            public CustomField CustomField
            {
                get { return Target.Get<CustomField>(CustomField.Tag); }
                set { Target.Set(value); }
            }
        }

        private class MyMessage : Message
        {
            public MyMessage()
            {
                MyTestComponent = new TestComponent(this);
            }

            public TestComponent MyTestComponent { get; }
        }

        [Test]
        public void SettingFieldOnComponentShouldDelegateToParentMessage()
        {
            var message = new MyMessage();
            message.MyTestComponent.CustomField = new CustomField(111);

            Assert.IsTrue(message.Contains(CustomField.Tag));
        }

        [Test]
        public void GettingFieldFromComponentShouldDelegateToParentMessage()
        {
            var message = new MyMessage();
            var field = message.MyTestComponent.CustomField;

            Assert.IsNull(field);
            message.Set(new CustomField(182));

            field = message.MyTestComponent.CustomField;

            Assert.IsNotNull(field);
            Assert.AreEqual(182, field.Value);
        }
    }
}
