﻿namespace FixNet.Core.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using Fields;
    using Parsing;
    using Specification;

    using NUnit.Framework;

    [TestFixture]
    public class ParserTests
    {
        [Test]
        public void ShouldProperlyExtractFieldsFromValidTextMessage()
        {
            var message = "12=A" + Delimiters.Ascii.Soh + "32=099231" + Delimiters.Ascii.Soh;
            var extracted = Parser.ExtractAllFields(message);
            Assert.AreEqual(new[] { new UntypedField(12, "A"), new UntypedField(32, "099231") }, extracted);
        }

        [Test]
        public void ShouldSkipIncompleteTrailingField()
        {
            var message = "12=A" + Delimiters.Ascii.Soh + "32=099231";
            var extracted = Parser.ExtractAllFields(message);
            Assert.AreEqual(new[] { new UntypedField(12, "A") }, extracted);
        }

        [Test]
        public void ShouldReturnEmptySequenceForInvalidMessageWithoutTags()
        {
            const string Message = "TestingIt";
            var extracted = Parser.ExtractAllFields(Message);
            Assert.IsEmpty(extracted);
        }

        [Test]
        public void ShouldCrashIfTheTagHasWrongType()
        {
            var message = "Test=A" + Delimiters.Ascii.Soh;
            Assert.Throws<FormatException>(() => Parser.ExtractAllFields(message).ToList());
        }

        [Test]
        public void CreatingParserWithNullSpecDataShouldThrow()
        {
            Assert.Throws<ArgumentNullException>(() => new Parser(null, null));
        }

        [Test]
        public void StringIsTheDefaultFieldTypeWhenSpecIsMissing()
        {
            var fields = new[] { new UntypedField(25, "DEF"), new UntypedField(32, "CAE") };
            var parser = new Parser(SpecificationData.Empty, new Dictionary<FieldType, CustomFieldParser>());
            var typed = fields.Select(parser.MakeTyped).ToList();
            foreach (var typedField in typed)
            {
                Assert.IsInstanceOf<Field.String>(typedField);
            }
        }

        [Test]
        public void StringIsTheDefaultFieldTypeForFieldsMissingFromTheSpec()
        {
            var fields = new[] { new UntypedField(12, "321"), new UntypedField(42, "Y") };

            var specData =
                new SpecificationData(
                    new Dictionary<int, SpecField>
                    {
                        [12] = new SpecField(12, "TST", FieldType.Int),
                    },
                    new Dictionary<MsgType, SpecElement>(),
                    null,
                    null);
            var parser = new Parser(specData, new Dictionary<FieldType, CustomFieldParser>());
            var typed = fields.Select(parser.MakeTyped).ToList();
            Assert.IsInstanceOf<Field.Int>(typed[0]);
            Assert.IsInstanceOf<Field.String>(typed[1]);

            Assert.AreEqual(321, ((Field.Int)typed[0]).Value);
            Assert.AreEqual("Y", ((Field.String)typed[1]).Value);
        }

        [Test]
        public void AppropriateFieldsShouldBeTypedAccordingToSpec()
        {
            var fields = new[] { new UntypedField(12, "321"), new UntypedField(42, "Y") };

            var specData =
                new SpecificationData(
                    new Dictionary<int, SpecField>
                        {
                            [12] = new SpecField(12, "TST", FieldType.Int),
                            [42] = new SpecField(42, "ANSWR", FieldType.Boolean)
                        },
                    new Dictionary<MsgType, SpecElement>(),
                    null,
                    null);
            var parser = new Parser(specData, new Dictionary<FieldType, CustomFieldParser>());
            var typed = fields.Select(parser.MakeTyped).ToList();
            Assert.IsInstanceOf<Field.Int>(typed[0]);
            Assert.IsInstanceOf<Field.Boolean>(typed[1]);

            Assert.AreEqual(321, ((Field.Int) typed[0]).Value);
            Assert.AreEqual(true, ((Field.Boolean)typed[1]).Value);
        }

        [Test]
        public void TryingToAssignFieldTypeFromUnsupportedFixTypeShouldThrow()
        {
            var fields = new[] { new UntypedField(12, "321") };

            var specData =
                new SpecificationData(
                    new Dictionary<int, SpecField>
                    {
                        [12] = new SpecField(12, "TST", new FieldType("NOT-SUPPORTED"))
                    },
                    new Dictionary<MsgType, SpecElement>(),
                    null,
                    null);
            var parser = new Parser(specData, new Dictionary<FieldType, CustomFieldParser>());
            Assert.Throws<NotSupportedException>(() => fields.Select(parser.MakeTyped).ToList());
        }
    }
}