﻿namespace FixNet.Core.Tests
{
    using System.Collections.Generic;

    using Fields;

    using Exceptions;
    using Specification;

    using static Delimiters.Ascii;

    using NUnit.Framework;
    using Parsing;

    [TestFixture]
    public class MessageBuilderTests
    {
        [Test]
        public void ShouldFailIfThereIsNoFixVersionField()
        {
            var message = $"12=312{Soh}";
            var builder = new MessageBuilder(SpecificationData.Empty, MessageBuilder.VerificationOptions.None,
                new Dictionary<FieldType, CustomFieldParser>());

            Assert.Throws<MessageParsingException>(() => builder.Build(message));
        }

        [Test]
        public void ShouldFailIfMsgTypeFieldIsMissing()
        {
            var message = $"8=FIX4.2{Soh}";
            var builder = new MessageBuilder(SpecificationData.Empty, MessageBuilder.VerificationOptions.None,
                new Dictionary<FieldType, CustomFieldParser>());

            Assert.Throws<MessageParsingException>(() => builder.Build(message));
        }

        [Test]
        public void ShouldParseMessageWithoutSpecAndWithoutVerification()
        {
            var message = $"8=FIX4.2{Soh}35=A{Soh}18=T{Soh}321=A{Soh}";
            var builder = new MessageBuilder(SpecificationData.Empty, MessageBuilder.VerificationOptions.None,
                new Dictionary<FieldType, CustomFieldParser>());
            var typedMessage = builder.Build(message);
            Assert.IsTrue(typedMessage.Contains(18));
            Assert.IsTrue(typedMessage.Contains(321));
        }

        [Test]
        public void TagsListedInHeaderOrderFieldsGoIntoHeaderEvenWithoutSpec()
        {
            var message = $"8=FIX4.2{Soh}9=15{Soh}35=A{Soh}18=T{Soh}321=A{Soh}";
            var builder = new MessageBuilder(SpecificationData.Empty, MessageBuilder.VerificationOptions.None,
                new Dictionary<FieldType, CustomFieldParser>());
            var typedMessage = builder.Build(message);
            foreach (var tag in typedMessage.Header.OrderedFieldTags)
            {
                Assert.IsTrue(typedMessage.Header.Contains(tag));
                Assert.IsFalse(typedMessage.Contains(tag));
            }
        }

        [Test]
        public void MessageSpecIsRequiredForVerification()
        {
            var message = $"8=FIX4.2{Soh}35=A{Soh}";
            var builder = new MessageBuilder(
                SpecificationData.Empty,
                MessageBuilder.VerificationOptions.CheckRequiredFields,
                new Dictionary<FieldType, CustomFieldParser>());
            Assert.Throws<VerificationPreconditionException>(() => builder.Build(message));
        }

        [Test]
        public void SpecificationHeaderHasToBeSetForVerification()
        {
            var message = $"8=FIX4.2{Soh}35=A{Soh}";
            var builder =
                new MessageBuilder(
                    new SpecificationData(
                        new Dictionary<int, SpecField>(),
                        new Dictionary<MsgType, SpecElement>
                            {
                                [new MsgType("A")] =
                                    new SpecElement(new[] { 42, 12, 32 }, new[] { 42 })
                            },
                        new SpecElement(new[] { 8 }, new[] { 8 }),
                        null),
                    MessageBuilder.VerificationOptions.CheckRequiredFields,
                    new Dictionary<FieldType, CustomFieldParser>());
            Assert.Throws<VerificationPreconditionException>(() => builder.Build(message));
        }

        [Test]
        public void SpecificationTrailerHasToBeSetForVerification()
        {
            var message = $"8=FIX4.2{Soh}35=A{Soh}";
            var builder =
                new MessageBuilder(
                    new SpecificationData(
                        new Dictionary<int, SpecField>(),
                        new Dictionary<MsgType, SpecElement>
                            {
                                [new MsgType("A")] =
                                    new SpecElement(new[] { 42, 12, 32 }, new[] { 42 })
                            },
                        null,
                        new SpecElement(new[] { 8 }, new[] { 8 })),
                    MessageBuilder.VerificationOptions.CheckRequiredFields,
                    new Dictionary<FieldType, CustomFieldParser>());
            Assert.Throws<VerificationPreconditionException>(() => builder.Build(message));
        }
    }
}
