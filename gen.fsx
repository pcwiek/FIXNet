﻿#r "packages/FSharp.Data/lib/net40/FSharp.Data.dll"
#r "System.Xml.Linq"

open FSharp.Data

type CoreCsprojXml = XmlProvider< "src/lib/FixNet.Core/FixNet.Core.csproj", Global=true >

type FixDeclXml = XmlProvider< "spec/sample.xml", SampleIsList=true, Global=true >

module Option = 
    let getOrElse other = 
        function 
        | Some(v) -> v
        | None -> other

type MessagesDestination = 
    { MessageTypesPath : string
      CsProjPath : string }

module Fields = 
    type FieldType = 
        | FieldType of string
    
    type ClrType = 
        | ClrType of string
    
    type SubDecl = 
        { ClrType : ClrType
          Value : string
          Description : string }
    
    type Field = 
        { Name : string
          Number : int
          Type : FieldType * ClrType
          SubDeclarations : SubDecl [] }
    
    let private textInfo = (new System.Globalization.CultureInfo("en-US", false)).TextInfo
    
    let private extractType (field : FixDeclXml.Field) = 
        match field.Type.Value with
        | "CHAR" -> FieldType "Char", ClrType "char"
        | "INT" | "NUMINGROUP" | "SEQNUM" | "LENGTH" -> FieldType "Int", ClrType "int"
        | "AMT" | "PERCENTAGE" | "PRICE" | "QTY" | "PRICEOFFSET" | "FLOAT" -> FieldType "Decimal", ClrType "decimal"
        | "BOOLEAN" -> FieldType "Boolean", ClrType "bool"
        | "UTCTIMESTAMP" | "TZTIMESTAMP" -> FieldType "DateTime", ClrType "System.DateTime"
        | "UTCDATEONLY" | "UTCDATE" | "DATE" -> FieldType "Date", ClrType "System.DateTime"
        | "TIME"  | "UTCTIMEONLY" | "TZTIMEONLY" -> FieldType "Time", ClrType "System.TimeSpan"
        | "STRING" | "MULTIPLEVALUESTRING" | "MULTIPLECHARVALUE" | "DATA" | "EXCHANGE" | "LOCALMKTDATE" | "MONTHYEAR" | "DAYOFMONTH" | "COUNTRY"  | "MULTIPLESTRINGVALUE" | "XMLDATA" | "LANGUAGE" | "CURRENCY" -> 
            FieldType "String", ClrType "string"
        | other -> failwithf "Unsupported field type: %s" other
    
    let private generateField (field : Field) = 
        let (FieldType tpe, ClrType underlying) = field.Type
        
        let createSubdecl subdecl = 
            let stringifiedValue = 
                match subdecl.ClrType with
                | ClrType "char" -> sprintf "'%s'" subdecl.Value
                | ClrType "int" -> sprintf "%s" subdecl.Value
                | ClrType "bool" -> 
                    match subdecl.Value with
                    | "Y" -> sprintf "%s" "true"
                    | "N" -> sprintf "%s" "false"
                    | other -> failwithf "Unsupported boolean value for field %s: %s" field.Name other
                | _ -> sprintf "\"%s\"" subdecl.Value
            sprintf "\t\tpublic static readonly %s %s = new %s(%s);" field.Name subdecl.Description field.Name 
                stringifiedValue
        
        let subdecls = field.SubDeclarations |> Array.map createSubdecl
        
        let tokens = 
            seq { 
                yield (sprintf "\tpublic sealed class %s : Field.%s" field.Name tpe)
                yield "\t{"
                yield "\t\t///<summary>Tag of the field</summary>"
                yield (sprintf "\t\tpublic new const int Tag = %d;" field.Number)
                match tpe with
                | "Time" | "DateTime" -> 
                    yield (sprintf "\t\tpublic %s(%s value, bool includeMilliseconds) : base(Tag, value, includeMilliseconds) {}" field.Name underlying)
                    yield (sprintf "\t\tpublic %s(%s value) : this(value, true) {}" field.Name underlying)    
                | _ ->  yield (sprintf "\t\tpublic %s(%s value) : base(Tag, value) {}" field.Name underlying)
                yield! subdecls
                yield "\t}\n"
            }
        System.String.Join(System.Environment.NewLine, tokens)
    
    let generateReplacementMap (fields : FixDeclXml.Field []) = 
        let replacements = 
            [ "ID", "Id"
              "MD", "Md"
              "CP", "Cp"
              "NT", "Nt"
              "IOI", "Ioi"
              "XML", "Xml"
              "CFI", "Cfi"
              "DK", "Dk"
              "URL", "Url"
              "RFQ", "Rfq"
              "FX", "Fx"
              "TS", "Ts"
              "TZ", "Tz"
              "EFP", "Efp"
              "GT", "Gt" ]
        fields
        |> Array.map 
               (fun f -> 
               (f.Name, replacements |> List.fold (fun (name : string) (orig, repl) -> name.Replace(orig, repl)) f.Name))
        |> Map.ofArray
    
    let extractDeclarations nameReplacements (fields : FixDeclXml.Field []) = 
        fields |> Array.map (fun f -> 
                      let tpe = f |> extractType
                      let (_, clrTpe) = tpe
                      { Name = nameReplacements |> Map.find f.Name
                        Type = tpe
                        Number = f.Number.Value
                        SubDeclarations = 
                            f.Values |> Array.choose (fun v -> 
                                            v.Description.String |> Option.map (fun desc -> 
                                                                        let startsWithDigit = 
                                                                            desc.[0] |> System.Char.IsDigit
                                                                        
                                                                        let subdecl = 
                                                                            { Description = 
                                                                                  (desc.ToLowerInvariant().Replace('_', ' ') |> textInfo.ToTitleCase)
                                                                                      .Replace(" ", System.String.Empty)
                                                                              Value = v.Enum.Value
                                                                              ClrType = clrTpe }
                                                                        if startsWithDigit then 
                                                                            { subdecl with Description = 
                                                                                               "_" + subdecl.Description }
                                                                        else subdecl)) })
    
    let generateAdminClasses fieldDeclarations = 
        seq { 
            yield sprintf "// Automatically generated @ %s" (System.DateTimeOffset.Now.ToString())
            yield sprintf "#pragma warning disable 108"
            yield sprintf "namespace FixNet.Core.Fields.Admin"
            yield "{"
            yield! fieldDeclarations |> Seq.map generateField
            yield "}"
        }
    
    let generateClasses fieldDeclarations = 
        seq { 
            yield sprintf "// Automatically generated @ %s" (System.DateTimeOffset.Now.ToString())
            yield sprintf "#pragma warning disable 108"
            yield sprintf "namespace FixNet.Core.Fields"
            yield "{"
            yield! fieldDeclarations |> Seq.map generateField
            yield "}"
        }

module Messages = 
    type Message = 
        { Name : string
          Fields : string []
          MsgType : string }
    
    type VersionedMessages = 
        { FixNamespace : string
          FixVersion : string
          Messages : Message [] }
    
    let private extractMessages (fixDecl : FixDeclXml.Fix) = 
        let replacements = 
            [ "RFQ", "Rfq"
              "ACK", "Ack"
              "IOI", "Ioi" ]
        
        let msg = 
            fixDecl.Messages |> Array.map (fun m -> 
                                    { Name = 
                                          replacements 
                                          |> List.fold (fun (name : string) (orig, repl) -> name.Replace(orig, repl)) 
                                                 m.Name
                                      MsgType = m.Msgtype.Value
                                      Fields = m.Fields |> Array.map (fun f -> f.Name) })
        
        let baseNs = sprintf "%d%d" fixDecl.Major fixDecl.Minor
        
        let sp = 
            fixDecl.Servicepack
            |> Option.filter (fun sp -> sp <> 0)
            |> Option.map (fun s -> sprintf "Sp%d" s)
            |> Option.getOrElse System.String.Empty
        
        let name = 
            fixDecl.Type
            |> Option.map 
                   (fun s -> (s.[0] |> System.Char.ToUpperInvariant).ToString() + s.Substring(1).ToLowerInvariant())
            |> Option.getOrElse "Fix"
        
        { FixNamespace = sprintf "%s%s%s" name baseNs sp
          FixVersion = sprintf "%s.%d.%d" (name.ToUpperInvariant()) fixDecl.Major fixDecl.Minor
          Messages = msg }
    
    let indent str = sprintf "\t%s" str
    
    let private generateField fieldName = 
        seq { 
            yield (sprintf "public %s %s" fieldName fieldName)
            yield "{"
            yield "\tget"
            yield "\t{"
            yield (sprintf "\t\treturn Get<%s>(%s.Tag);" fieldName fieldName)
            yield "\t}"
            yield "\tset"
            yield "\t{"
            yield "\t\tSet(value);"
            yield "\t}"
            yield "}"
            yield ""
        }
    
    let private generateBaseClass fixVersion allMessages = 
        let factoryFunc = 
            seq { 
                yield "public static Message Create(MsgType messageType)"
                yield "{"
                yield "\tswitch (messageType.Value)"
                yield "\t{"
                yield! allMessages
                       |> Array.map (fun m -> sprintf "case %s.MessageType: return new %s();" m.Name m.Name)
                       |> Seq.ofArray
                       |> Seq.map (indent >> indent)
                yield "\t}"
                yield "\treturn new Message();"
                yield "}"
            }
        seq { 
            yield "public class Message : Messages.Message"
            yield "{"
            yield "\tpublic Message()"
            yield "\t{"
            yield (sprintf "\t\tHeader.Set(new BeginString(\"%s\"));" fixVersion)
            yield "\t}"
            yield! factoryFunc |> Seq.map indent
            yield "}"
        }
    
    let private generateClass message replacements = 
        seq { 
            yield (sprintf "public class %s : Message" message.Name)
            yield "{"
            yield (sprintf "\tpublic const string MessageType = \"%s\";" message.MsgType)
            yield ""
            yield (sprintf "\tpublic %s()" message.Name)
            yield "\t{"
            yield "\t\tHeader.MsgType = new MsgType(MessageType);"
            yield "\t}"
            yield ""
            yield! (message.Fields
                    |> Seq.map (fun f -> replacements |> Map.find f)
                    |> Seq.collect generateField
                    |> Seq.map indent)
            yield "}"
        }
    
    let generateClasses replacements fix = 
        let extract = extractMessages fix
        extract.Messages
        |> Array.map (fun msg -> 
               (extract.FixNamespace, sprintf "%s.cs" msg.Name, 
                seq { 
                    yield (sprintf "namespace FixNet.Core.Messages.%s" extract.FixNamespace)
                    yield "{"
                    yield "\tusing Fields;"
                    yield "\t"
                    yield! (generateClass msg replacements |> Seq.map indent)
                    yield "}"
                }))
        |> Array.append ((extract.FixNamespace, "Message.cs", 
                          seq { 
                              yield (sprintf "namespace FixNet.Core.Messages.%s" extract.FixNamespace)
                              yield "{"
                              yield "\tusing Fields;"
                              yield "\t"
                              yield! (generateBaseClass extract.FixVersion extract.Messages |> Seq.map indent)
                              yield "}"
                          })
                         |> Array.singleton)

module FileGen = 
    open System.IO
    open System.Linq
    open System.Xml.Linq
    
    let patchReferences fieldsCsFile (generatedContent : seq<string * string * seq<string>>) (csProjPath : string) = 
        let csproj = csProjPath |> CoreCsprojXml.Load
        let rootElement = csproj.XElement
        let ns = rootElement.Name.Namespace.NamespaceName
        let name n = XName.Get(n, ns)
        let compileItemGroup = 
            rootElement.Elements(name "ItemGroup").Where(fun n -> n.Descendants(name "Compile").Any()).First()
        compileItemGroup.Elements().Where(fun c -> c.Attribute(XName.Get "Include").Value = "Fields\Fields.cs").Remove()
        compileItemGroup.Elements().Where(fun c -> c.Attribute(XName.Get "Include").Value.StartsWith("Messages\Fix"))
                        .Remove()
        let template = compileItemGroup.Elements().First()
        
        let compileFieldsItem path = 
            let element = XElement(template)
            element.Attribute(XName.Get "Include").SetValue(path)
            element
        
        let compileMessagesItem ns path = 
            let item = XElement(template)
            item.Attribute(XName.Get "Include").SetValue(sprintf "Messages\\%s\\%s" ns path)
            item
        
        compileItemGroup.Add(compileFieldsItem "Fields\Fields.cs")
        compileItemGroup.Add(generatedContent |> Seq.map (fun (ns, fname, _) -> compileMessagesItem ns fname))
        let writerSettings = System.Xml.XmlWriterSettings(Indent = true)
        use xmlWriter = System.Xml.XmlWriter.Create(csProjPath, writerSettings)
        rootElement.WriteTo(xmlWriter)
    
    let writeAllClasses messagesRootDir (generatedClasses : seq<string * string * seq<string>>) = 
        generatedClasses
        |> Seq.groupBy (fun (fixNs, _, _) -> fixNs)
        |> Seq.iter 
               (fun (ns, classes) -> 
               let target = System.IO.Path.Combine(messagesRootDir, ns)
               if not (Directory.Exists target) then Directory.CreateDirectory target |> ignore
               classes 
               |> Seq.iter 
                      (fun (_, filename, contents) -> 
                      File.WriteAllLines(System.IO.Path.Combine(target, filename), contents)))
    
    let deleteMessages allDirs = 
        allDirs |> Seq.iter (fun d -> 
                       try 
                           Directory.Delete(d, true)
                       with :? DirectoryNotFoundException -> ())
    
    let run (specFiles : seq<string>) fieldsTargetDir destination (csProjPath : string) = 
        let fieldDecl = 
            specFiles
            |> Seq.map FixDeclXml.Load
            |> Seq.cache
        
        let fieldNameReplacements = 
            fieldDecl
            |> Seq.collect (fun f -> f.Fields)
            |> Seq.distinctBy (fun a -> a.Name)
            |> Seq.toArray
            |> Fields.generateReplacementMap
        
        let allFieldDeclarations = 
            fieldDecl
            |> Seq.collect (fun p -> p.Fields |> Fields.extractDeclarations fieldNameReplacements)
            |> Seq.distinctBy (fun f -> (f.Number, f.Name))
            |> Seq.cache
        
        let fields = 
            allFieldDeclarations
            |> Seq.sortBy (fun f -> f.Name)
            |> Fields.generateClasses
        
        let messages = 
            fieldDecl
            |> Seq.collect (Messages.generateClasses fieldNameReplacements)
            |> Seq.cache
        
        if not (Directory.Exists fieldsTargetDir) then Directory.CreateDirectory fieldsTargetDir |> ignore
        File.WriteAllLines(System.IO.Path.Combine(fieldsTargetDir, "Fields.cs"), fields)
        messages |> writeAllClasses destination.MessageTypesPath
        patchReferences "Fields/Fields.cs" messages csProjPath
