## FIX.NET

[FIX](http://www.fixtradingcommunity.org/) <sup>[1](https://en.wikipedia.org/wiki/Financial_Information_eXchange)</sup> engine for .NET

Loosely based on the [QuickFix/n](https://github.com/connamara/quickfixn) and [quickfixj](http://www.quickfixj.org/), but with a different API and implementation approach.


#### License

Licensed under MIT. See [here](license.txt).