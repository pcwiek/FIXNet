// include Fake lib
#r "packages/FAKE/tools/FakeLib.dll"

open Fake
open Fake.Testing
open System.Linq

RestorePackages()

// Properties
let coreBuildDir = "./build/"
let buildDir = coreBuildDir @@ "/lib/"
let testDir = coreBuildDir @@ "/test/"
let genDir = coreBuildDir @@ "/gen/"
let benchDir = coreBuildDir @@ "/bench/"

#load "gen.fsx"
open Gen

let version = 
    match buildServer with
    | BuildServer.AppVeyor -> buildVersion
    | _ -> "0.1"

// Targets
Target "Clean" (fun _ -> CleanDirs [ buildDir; testDir; genDir ])

(*
Target "RunGenerator" (fun _ ->
    trace "Running generator" 
    trace "Generating fields and messages..."
    let rootPath = "src/lib/FixNet.Core"
    let specFiles  = (!!"spec/FIX*.xml") |> Seq.rev
    FileGen.deleteMessages (!!"**/FixNet.Core/Messages/Fix*/")
    FileGen.run specFiles (rootPath @@ "Fields") {MessageTypesPath = rootPath @@ "Messages"; CsProjPath = rootPath } (!!(rootPath + "/*FixNet.Core.csproj") |> Seq.head)
    trace "Generation successful" )
*)

Target "BuildBenchmarks" (fun _ -> 
    !!"src/bench/**/*.csproj"
    |> MSBuildRelease benchDir "Build"
    |> Log "BenchBuild-Output: ")
Target "BuildLib" (fun _ -> 
    !!"src/lib/**/*.csproj"
    |> MSBuildRelease buildDir "Build"
    |> Log "LibBuild-Output: ")
Target "BuildTest" (fun _ -> 
    !!"src/test/**/*.csproj"
    |> MSBuildDebug testDir "Build"
    |> Log "TestBuild-Output: ")
Target "Test" (fun _ -> 
    !!(testDir + "/*.Test*.dll") |> NUnit3(fun p -> { p with ShadowCopy = false
                                                             WorkingDir = testDir }))
Target "Default" DoNothing
Target "BuildCI" DoNothing

// Dependencies
(*"Clean" ==> "RunGenerator"*)

"Clean" ==> "BuildLib" ==> "BuildTest" ==> "Test" ==> "Default"
"Test" ==> "BuildBenchmarks"

// This one will be used by CI server, since the tests are discovered automatically and in a separate step.
"Clean" ==> "BuildLib" ==> "BuildTest" ==> "BuildCI"

// start build
RunTargetOrDefault "Default"
